<?php

/**
 * Project:   ePochta Reseller
 * File:      bootstrap.php
 * Date:      16.07.2012
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Main application bootstrapping.
 *
 * @author    Victor Burak <vb@atompark.com>
 */

/**
 * Set the default time zone, because the server's time may be varied.
 */
date_default_timezone_set('Europe/Kiev');

/**
 * If session not being started automatically
 * and if there is no active session identificator,
 * we start new session.
 */

if (!get_cfg_var('session.auto_start')) {
    $sessionId = session_id();
    if (empty($sessionId)) {
        session_name('EPOCHTA_SESSION');
        session_start();
    }
}

/**
 * Set default permissinons for file creating at 664.
 */
umask(0002);

/**
 * This for PHP 5.2 only, where is no __DIR__ directive.
 */
if (!defined('__DIR__')) {
    define('__DIR__', dirname(__FILE__));
}

/**
 * Define some constants for system pathes.
 */
define('DS', DIRECTORY_SEPARATOR);
define('BT_DOC_ROOT', realpath(__DIR__ . DS . '..' . DS) . DS);
define('BT_APP_PATH', BT_DOC_ROOT . 'app' . DS);
define('BT_LOG_PATH', BT_DOC_ROOT . 'log' . DS);

define('BT_CONTROLLERS_PATH', BT_APP_PATH . 'controllers' . DS);
define('BT_MODELS_PATH', BT_APP_PATH . 'models' . DS);
define('BT_MAPPERS_PATH', BT_MODELS_PATH . 'mappers' . DS);
define('BT_VIEWS_PATH', BT_APP_PATH . 'views' . DS);
define('BT_LAYOUTS_PATH', BT_APP_PATH . 'layouts' . DS);

define('BT_MODULES_PATH', BT_APP_PATH . 'modules' . DS);

define('BT_LIB_PATH', BT_DOC_ROOT . 'lib' . DS);
define('BT_BTOOLS_PATH', BT_LIB_PATH . 'BTools' . DS);
define('BT_VENDOR_PATH', BT_LIB_PATH . 'Vendors' . DS);

define('BT_KINT_PATH', BT_VENDOR_PATH . 'Kint' . DS);
define('BT_SMTP_PATH', BT_VENDOR_PATH . 'SMTP' . DS);

define('BT_CSS_PATH', BT_APP_PATH . 'assets' . DS . 'css' . DS);
define('BT_IMG_PATH', BT_APP_PATH . 'assets' . DS . 'img' . DS);
define('BT_JS_PATH', BT_APP_PATH . 'assets' . DS . 'js' . DS);

/**
 * Tools for convenient debug output.
 */
if (BT_DEBUG_MODE) {
    require_once(BT_BTOOLS_PATH . 'pr.php');
    require_once(BT_KINT_PATH . 'Kint.class.php');
}

/**
 * Register our autoloader.
 */
require_once(BT_BTOOLS_PATH . 'BAutoloader.php');

BAutoloader::addPath(
    BT_APP_PATH,
    BT_BTOOLS_PATH,
    BT_SMTP_PATH,
    BT_CONTROLLERS_PATH,
    BT_MODELS_PATH,
    BT_MAPPERS_PATH
);
BAutoloader::register();

/**
 * Initialize and start the application.
 */
$configFile = BT_APP_PATH . 'config.ini';

/** @var $app BApplication */
$app = BApplication::getInstance();
$app->init($configFile);

BFrontController::getInstance()->registerPlugin(new FCAuthHelper());
BFrontController::getInstance()->registerPlugin(new FCResponseHelper());
BFrontController::getInstance()->registerPlugin(new FCLayoutInit());

// initialize and store system database adapter
$dbCredentials = array(
    'dbHost' => $app->getRegistry()->get('db.host'),
    'dbUser' => $app->getRegistry()->get('db.user'),
    'dbPass' => $app->getRegistry()->get('db.pass'),
    'dbName' => $app->getRegistry()->get('db.name'),
    'dbPort' => $app->getRegistry()->get('db.port'),
);
//$app->getRegistry()->set('db', BDatabaseS::getInstance($dbCredentials));
$app->getRegistry()->set('db', new BDatabaseNS($dbCredentials, 'latin1'));

// initialize and store system SMTP mailer
$smtpCredentials = array(
    'smtpHost' => $app->getRegistry()->get('smtp.host'),
    'smtpUser' => $app->getRegistry()->get('smtp.user'),
    'smtpPass' => $app->getRegistry()->get('smtp.pass'),
    'smtpPort' => $app->getRegistry()->get('smtp.port'),
);
//$app->getRegistry()->set('MAIL', new BMail($smtpCredentials));

// initialize and store system logger
$logFileName = BT_LOG_PATH . $app->getRegistry()->get('log.filename');
$app->getRegistry()->set('log', new BFileLogger($logFileName));

$app->getRegistry()->set('auth', new ResellerAuth('UserModel', 'Email', 'Password'));
$app->getRegistry()->set('acl', new BAcl(BT_APP_PATH . $app->getRegistry()->get('acl.filename')));


//$app->registerModule('admin');

$app->run();
