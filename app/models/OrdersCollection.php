<?php
/**
 * Project:   Atomic Reseller
 * File:      OrdersCollection.php
 * Date:      29.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrdersCollection
 *
 * @uses      BCollectionBase
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class OrdersCollection extends BCollectionBase
{
    public function __construct() {
        parent::__construct('OrderModel');
    }
}
