<?php
/**
 * Project:   Atomic Reseller
 * File:      SuperModel.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of SuperModel
 *
 * @uses      BDomainModel
 * @author    Victor Burak <vb@atompark.com>
 */
class SuperModel extends BDomainModel
{

}
