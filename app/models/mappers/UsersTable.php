<?php
/**
 * Project:   Atomic Reseller
 * File:      UsersTable.php
 * Date:      21.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of UsersTable
 *
 * @uses      BDbTable
 * @author    Victor Burak <vb@atompark.com>
 */
class UsersTable extends BDbTable
{
    public function __construct() {
        $this->setAdapter(BApplication::getInstance()->getRegistry()->get('db'));
        $this->setTableName(BApplication::getInstance()->getRegistry()->get('table.users.name'));
        $this->setPrimaryKey(BApplication::getInstance()->getRegistry()->get('table.users.pkey'));

        parent::__construct();
    }

}
