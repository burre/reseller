<?php
/**
 * Project:   Atomic Reseller
 * File:      OrderProductTableTable.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrderProductTable
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class OrderProductTable extends BDbTable
{
    public function __construct() {
        $this->setAdapter(BApplication::getInstance()->getRegistry()->get('db'));
        $this->setTableName(BApplication::getInstance()->getRegistry()->get('table.order_product.name'));
        $this->setPrimaryKey(BApplication::getInstance()->getRegistry()->get('table.order_product.pkey'));

        parent::__construct();
    }

}
