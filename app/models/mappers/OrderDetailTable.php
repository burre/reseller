<?php
/**
 * Project:   ${project.name}
 * File:      OrderDetailTable.php
 * Date:      28.08.12
 *
 * @package   ${package}
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrderDetailTable
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class OrderDetailTable extends BDbTable
{
    public function __construct() {
        $this->setAdapter(BApplication::getInstance()->getRegistry()->get('db'));
        $this->setTableName(BApplication::getInstance()->getRegistry()->get('table.order_detail.name'));
        $this->setPrimaryKey(BApplication::getInstance()->getRegistry()->get('table.order_detail.pkey'));

        parent::__construct();
    }

}
