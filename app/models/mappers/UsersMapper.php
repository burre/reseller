<?php
/**
 * Project:   Atomic Reseller
 * File:      UsersMapper.php
 * Date:      21.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of UsersMapper
 *
 * @uses      BDBMapperAbstract
 * @author    Victor Burak <vb@atompark.com>
 */
class UsersMapper extends BDBMapperAbstract
{
    public function __construct() {
        $this->setDbTable(new UsersTable())->initMapperData();
    }

}
