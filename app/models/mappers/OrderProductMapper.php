<?php
/**
 * Project:   Atomic Reseller
 * File:      OrderProductMapper.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrderProductMapper
 *
 * @uses      BDBMapperAbstract
 * @author    Victor Burak <vb@atompark.com>
 */
class OrderProductMapper extends BDBMapperAbstract
{
    public function __construct() {
        $this->setDbTable(new OrderProductTable())->initMapperData();
    }

}
