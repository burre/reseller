<?php
/**
 * Project:   Atomic Reseller
 * File:      OrderDetailMapper.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrderDetailMapper
 *
 * @uses      BDBMapperAbstract
 * @author    Victor Burak <vb@atompark.com>
 */
class OrderDetailMapper extends BDBMapperAbstract
{
    public function __construct() {
        $this->setDbTable(new OrderDetailTable())->initMapperData();
    }

    /**
     * @param stdClass $filter
     * @param bool     $paginator
     *
     * @return array|stdClass
     */
    public function getOrders(stdClass $filter, $paginator = FALSE) {
        $primaryKey = $this->getDbTable()->getPrimaryKey();
        $order      = "`{$primaryKey}` ";

        if ($filter->daterangeOn) {
            $from = BHelpers::mysqlDate(strtotime($filter->from));
            $till = BHelpers::mysqlDate(strtotime($filter->till));
        }

        $where  = "`order_status` = {$filter->typeId} ";
        $where .= isset($filter->userId) ? "AND `Site` = {$filter->userId} " : '';
        $where .= isset($from) ? "AND `order_date` >= '{$from}' " : '';
        $where .= isset($till) ? "AND `order_date` <= '{$till}' " : '';
        $where .= isset($filter->keywords) && !empty($filter->keywords)
            ? "AND (`order_num` LIKE '%{$filter->keywords}%'
            OR `user_mail` LIKE '%{$filter->keywords}%'
            OR `user_name1` LIKE '%{$filter->keywords}%'
            OR `user_name2` LIKE '%{$filter->keywords}%'
            OR `user_name3` LIKE '%{$filter->keywords}%'
            OR `user_org` LIKE '%{$filter->keywords}%') "
            : '';

        if ($paginator) {
            /**
             * This part calculate result rows for the filter conditions.
             */
            $select = "COUNT(`{$primaryKey}`) AS `rows`";
            $table  = $this->getDbTable()->getTableName();
            $sql    = "SELECT {$select} FROM `{$table}` WHERE {$where}";

            $this->getDbTable()->getAdapter()->query($sql);

            return $this->getDbTable()->getAdapter()->getRowAsObject();
        } else {
            /**
             * This part returns the query result.
             */
            $order = isset($filter->orderBy) ? "`{$filter->orderBy}` " : $order;
            $order .= isset($filter->direct) ? "{$filter->direct}" : 'DESC';

            $limit = '';
            if (isset($filter->limitOffset) && isset($filter->limitRows)) {
                $limit .= "{$filter->limitOffset}, {$filter->limitRows}";
            } elseif (isset($filter->limitRows)) {
                $limit .= "{$filter->limitRows}";
            }

            return $this->getDbTable()->select($where, $order, $limit);
        }
    }

}
