<?php
/**
 * Project:   Atomic Reeller
 * File:      UsersCollection.php
 * Date:      21.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of UsersCollection
 *
 * @uses
 * @author    Victor Burak <vb@atompark.com>
 */
class UsersCollection extends BCollectionBase
{
    public function __construct() {
        parent::__construct('UserModel');
    }

}
