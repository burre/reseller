<?php
/**
 * Project:   Atomic Reseller
 * File:      UserModel.php
 * Date:      21.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of UserModel
 *
 * @uses      BDomainModel
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class UserModel extends SuperModel implements BUserInterface
{
    const MANAGER = 0;
    const ADMIN   = 1;
    const MEMBER  = 2;
    const BOOKER  = 3;
    const GUEST   = 5;

    private $_role = UserModel::GUEST;
    private $_loggedIn = FALSE;

    public function __construct() {
        $this->_collectionType = 'UsersCollection';

        $this->_addMapper('users', new UsersMapper());
        $this->_setData($this->_getMapper('users')->getMapperData());
    }

    /**
     * Get all users.
     *
     * @return BCollectionBase
     */
    public function getAll() {
        /** @var $mapper UserMapper */
        $mapper = $this->_getMapper('users');
        $where  = '1';
        $order  = $mapper->getDbTable()->getPrimaryKey();
        $result = $mapper->getDbTable()->select($where, $order);

        $collection = $this->_makeCollection($result);

        d($this->_getMapper('users'));

        return $collection;
    }

    /**
     * Get user role.
     *
     * @return string
     */
    public function getRole() {
        $this->_role = $this->User_status ? (int)$this->User_status : UserModel::GUEST;
        $retval      = '';

        switch ($this->_role) {
            case 0:
                $retval = 'manager';
                break;
            case 1:
                $retval = 'admin';
                break;
            case 2:
                $retval = 'member';
                break;
            case 3:
                $retval = 'booker';
                break;
            case 5:
                $retval = 'guest';
                break;
        }

        return $retval;
    }

    /**
     * Get login status.
     *
     * @return bool
     */
    public function isLoggedIn() {
        return $this->_loggedIn;
    }

    /**
     * Set login status.
     *
     * @param bool $status
     *
     * @return UserModel
     */
    public function setLoggedIn($status = TRUE) {
        $this->_loggedIn = $status;
        return $this;
    }

    /**
     * Set user role.
     *
     * @param $role
     */
    public function setRole($role) {
        // TODO: Implement setRole() method.
    }

    public function getName() {
        return $this->FirstName . ' ' . $this->LastName;
    }

    public function getLogin() {
        return $this->Email;
    }
}
