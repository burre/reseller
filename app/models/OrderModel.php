<?php
/**
 * Project:   Atomic Reseller
 * File:      OrderModel.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrderModel
 *
 * @uses      SuperModel
 * @author    Victor Burak <vb@atompark.com>
 */
class OrderModel extends SuperModel
{
    const UNPAID   = 1;
    const PAID     = 2;
    const DELETED  = 3;
    const CANCELED = 4;

    const PAGINATOR = TRUE;

    /**
     * Class constructor.
     */
    public function __construct() {
        $this->_collectionType = 'OrdersCollection';

        $this->_addMapper('order_detail', new OrderDetailMapper());
        $this->_setData($this->_getMapper('order_detail')->getMapperData());

        $this->_addMapper('order_product', new OrderProductMapper());
        //$this->_setData($this->_getMapper('order_product')->getMapperData());
    }

    /**
     * Get the collection of orders.
     *
     * @param stdClass $filter
     *
     * @return BCollectionBase
     */
    public function getOrders(stdClass $filter) {
        if ($result = $this->_getMapper('order_detail')->getOrders($filter)) {
            return $this->_makeCollection($result);
        }
    }

    /**
     * Returns the paginator object and updates the $queryParams object.
     *
     * @param stdClass $filter
     *
     * @return BPaginator
     */
    public function getOrdersPaginator(stdClass $filter) {
        $result = $this->_getMapper('order_detail')->getOrders($filter, self::PAGINATOR);

        $paginator = new BPaginator($filter);
        $paginator->setItemsTotal($result->rows);
        $paginator->setCurrentPage($filter->page);
        $paginator->setItemsOnPage($filter->limitRows);
        //$paginator->setAutoHide(TRUE);

        return $paginator;
    }
}
