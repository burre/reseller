<?php
/**
 * Project:   BTools
 * File:      FCAuthHelper.php
 * Date:      1.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of FCAuthHelper
 *
 * @uses      BFrontControllerPluginAbstract
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */

class FCAuthHelper extends BFrontControllerPluginAbstract
{
    /** @var $auth ResellerAuth */
    protected $_auth = NULL;

    /** @var $acl BAcl */
    protected $_acl = NULL;

    /** @var $acl BUserInterface */
    protected $_user = NULL;

    /**
     * Before start the dispatch loop in the FrontController
     * we'll check the login status of user and set flag to log in if need.
     * If user is logged in (by session) we do authenticate user and logging in.
     */
    public function dispatchLoopStartup() {
        $this->_auth = $this->_app->getRegistry()->get('auth');
        $this->_acl  = $this->_app->getRegistry()->get('acl');
        $this->_user = new UserModel();
        $this->_app->getRegistry()->set('user', $this->_user);

        if ($this->getRequest()->getSession('isLoggedIn')) {
            if ($this->_user = $this->_auth->getIdentity($this->getRequest()->getSession('login'))) {
                $this->_auth->doLogIn($this->_user);
            }
        } else {
            $this->getRequest()->delSession('isLoggedIn');
            $this->getRequest()->delSession('login');
            $this->getRequest()->delSession('loginTime');
        }
    }

    /**
     * On the pre-dispatch of any request in the FrontController
     * we'll check an user authentication and propose to log in if need.
     */
    public function preDispatch() {
        $role     = $this->_user->getRole();
        $route    = $this->_front->getRouter()->getRoute();
        $resource = $route->assembleUrl();

        $allowed = $this->_acl->checkResource($role, $resource);

        /**
         * Aulo-popup login dialog must be shown once only after 302 redirect.
         * The session variable 'needLogin' is needed for pop-up login dialog
         * and gets used is FCLayoutInit::routeStartup().
         * @see FCLayoutInit::routeStartup()
         */
        if ($this->getRequest()->getSession('needLogin') &&
            $this->getRequest()->getSession('isRedirect') &&
            $allowed
        ) {
            $this->getRequest()->delSession('isRedirect');
        } else {
            $this->getRequest()->delSession('needLogin');
            $this->getRequest()->delSession('isRedirect');
        }

        /**
         * If resource is not allowed we do next:
         * - for logged in users - send forbidden code;
         * - for not logged in users - send redirect to home page and request login.
         */
        if (!$allowed) {
            if ($this->_user->isLoggedIn()) {
                $this->_front->getResponse()->setResponseCode('403');
                $this->getRequest()->setDispatched();
            } else {

                $this->getRequest()->setSession('isRedirect', TRUE);
                $this->getRequest()->setSession('needLogin', TRUE);
                $this->_front->redirect($this->_app->getBaseUrl() . '/');
            }
        }
    }

}

