<?php
/**
 * Project:   Atomic Reseller
 * File:      FCLayoutInit.php
 * Date:      27.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of FCLayoutInit
 *
 * @uses      BFrontControllerPluginAbstract
 * @author    Victor Burak <vb@atompark.com>
 */
class FCLayoutInit extends BFrontControllerPluginAbstract
{
    /**
     * Here we initialize the default layout with some variables.
     */
    public function routeStartup() {
        $layout = $this->_app->getLayout();

        $layout->baseUrl = $this->_app->getRequest()->protocol();
        $layout->baseUrl .= $this->_app->getRequest()->host();
        $layout->baseUrl .= $this->_app->getRegistry()->get('routes.base');

        $layout->routerBase = $this->_app->getRegistry()->get('routes.base');

        $layout->url = $this->_app->getRequest()->url();

        /**
         * Auto pop-up login dialog
         * It's provided by the JavaScript's event 'document ready'.
         *
         * @see assets/js/common.js
         */
        if ($this->getRequest()->getSession('needLogin')) {
            $this->_app->getLayout()->setNeedLogin(TRUE);
        } else {
            $this->_app->getLayout()->setNeedLogin(FALSE);
        }
    }

    /**
     * Here we initialize the default layout with some variables.
     */
    public function postDispatch() {
        $layout       = $this->_app->getLayout();
        $layout->user = $this->_app->getRegistry()->get('user');
    }
}
