<?php
/**
 * Project:   Atomic Reseller
 * File:      FCResponseHelper.php
 * Date:      27.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of FCResponseHelper
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class FCResponseHelper extends BFrontControllerPluginAbstract
{

    /**
     * Here we process the 403 and 404 response code.
     * View files (phtml) should be placed in 'blocks' directory of default layout.
     */
    public function postDispatch() {
        // 403 forbidden
        if ($this->_front->getResponse()->getResponseCode() == '403') {
            $content = $this->_app->getLayout()->parseBlock('403.phtml');
            $this->_front->getResponse()->appendBody('content', $content);
        }

        // 404 not found
        if ($this->_front->getResponse()->getResponseCode() == '404') {
            $content = $this->_app->getLayout()->parseBlock('404.phtml');
            $this->_front->getResponse()->appendBody('content', $content);
        }
    }

}
