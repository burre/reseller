<?php
/**
 * Project:   Atomic Reseller
 * File:      ResellerAuth.php
 * Date:      24.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of ResellerAuth
 *
 * @uses      BAuth
 * @author    Victor Burak <vb@atompark.com>
 */
class ResellerAuth extends BAuth
{
    /**
     * @var BApplication
     */
    protected $_app = NULL;

    /**
     * @var BFrontController
     */
    protected $_front = NULL;

    public function init() {
        $this->_app   = BApplication::getInstance();
        $this->_front = BFrontController::getInstance();
    }

    /**
     * Set user logged in.
     *
     * @param BUserInterface $user
     * @return \BUserInterface
     */
    public function doLogIn(BUserInterface $user) {
        $user->setLoggedIn();
        $this->_app->getRegistry()->set('user', $user);
        $identityField = $this->identityField;

        $this->_app->getRequest()->setSession('login', $user->$identityField);
        $this->_app->getRequest()->setSession('isLoggedIn', TRUE);
        $this->_app->getRequest()->setSession('loginTime', time());
        if ($this->_app->getRequest()->getSession('needLogin')) {
            $this->_app->getRequest()->delSession('needLogin');
        }

        return $user;
    }

    /**
     * Do log out for current user.
     */
    public function doLogOut() {
        $this->_app->getRegistry()->set('user', new UserModel());

        $this->_app->getRequest()->delSession('login');
        $this->_app->getRequest()->delSession('isLoggedIn');
        $this->_app->getRequest()->delSession('loginTime');
    }

}
