<?php
/**
 * Project:   Atomic Reseller
 * File:      CouponsController.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of CouponsController
 *
 * @uses      SuperController
 * @author    Victor Burak <vb@atompark.com>
 */
class CouponsController extends SuperController
{
    public function indexAction() {}

}
