<?php
/**
 * Project:   Atomic Reseller
 * File:      InvoicesController.php
 * Date:      28.08.12
 *
 * @package   ${package}
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of InvoicesController
 *
 * @uses      SuperController
 * @author    Victor Burak <vb@atompark.com>
 */
class InvoicesController extends SuperController
{

}
