<?php
/**
 * Project:   Atomic Reseller
 * File:      MemberControllerer.php
 * Date:      23.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of MemberController
 *
 * @uses      SuperController
 * @author    Victor Burak <vb@atompark.com>
 */
class MemberController extends SuperController
{
    /**
     * @var array
     */
    protected $_landingPages = array(
        'member'  => '/orders/index/type/paid/',
        'manager' => '/',
        'booker'  => '/orders/index/',
        'admin'   => '/admin/index/index/',
        'default' => '/',
    );


    public function loginAction() {
        $this->disableView();

        if ($this->getRequest()->isPost()) {
            $this->app->disableLayout();
            $this->disableView();

            $post = $this->getRequest()->getPost();
            $post = (object)BHelpers::clearUserInputArray((array)$post);

            /** @var $auth ResellerAuth */
            $auth = $this->app->getRegistry()->get('auth');

            if ($auth->authenticate($post->login, $post->password)) {
                $user = $auth->doLogIn($auth->getIdentity($post->login));
                $this->redirect($this->_getLandingPage($user->getRole()));
            }
        }
    }

    public function logoutAction() {
        $this->disableView();

        /** @var $auth ResellerAuth */
        $auth = $this->app->getRegistry()->get('auth');
        $auth->doLogOut($this->app->getRegistry()->get('user'));
        $this->redirect($this->baseUrl . '/');
    }

    public function restorePasswordAction() {
    }

    public function profileAction() {
        $this->view->user = $this->app->getRegistry()->get('user');
    }

    protected function _getLandingPage($role = 'default') {
        return $this->app->getBaseUrl() . $this->_landingPages[$role];
    }

}
