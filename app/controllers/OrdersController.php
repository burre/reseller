<?php
/**
 * Project:   Atomic Reseller
 * File:      OrdersController.php
 * Date:      28.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of OrdersController
 *
 * @uses      SuperController
 * @author    Victor Burak <vb@atompark.com>
 */
class OrdersController extends SuperController
{
    public $rowsOnPage = 10;

    /**
     * Show orders list.
     *
     * @param BObject $params
     */
    public function indexAction(BObject $params) {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $params->setData((array)$post);
        }

        $filter = $this->_initFilter($params);

        //s($filter);
        $order     = new OrderModel();
        $paginator = $order->getOrdersPaginator($filter);

        $paginator->setBaseUrl($this->baseUrl . '/orders/index/type/' . $filter->type . '/filter/' . $this->_encodeFilter($filter) . '/');
        $paginator->setView($this->view);
        $paginator->setTplBlockName('paginator.phtml');

        //d($paginator);
        $paginator->getRangeStart();

        $filter->limitOffset = $paginator->getLimitOffset();

        $this->view->collection = $order->getOrders($filter);
        $this->view->paginator  = $paginator->parse();
    }

    /**
     * Edit order.
     *
     * @param BObject $params
     */
    public function editAction(BObject $params) {

        if (isset($params->id)) {
            $order = new OrderModel();
            if ($order->findOneBy('order_id', $params->id)) {
                $this->view->order = $order;
            }
        }
    }

    public function init() {
        parent::init();

    }

    /**
     * Initialize filter and form.
     *
     * $filter->type
     * $filter->typeId
     * $filter->switch
     * $filter->daterangeOn
     * $filter->from
     * $filter->till
     * $filter->keywords
     * $filter->orderBy
     * $filter->direct
     * $filter->limitOffset
     * $filter->limitRows
     * $filter->page
     *
     * @param BObject $params
     *
     * @return stdClass
     */
    protected function _initFilter(BObject $params) {
        $defFrom = date('m') . '/01/' . date('Y');
        $defTill = date('m/d/Y');

        if (isset($params->filter)) {
            $filter = $this->_decodeFilter($params->filter);

            if ($filter->daterangeOn) {
                $filter->from         = isset($filter->from) ? $filter->from : $defFrom;
                $filter->till         = isset($filter->till) ? $filter->till : $defTill;
                $this->view->dateFrom = $filter->from;
                $this->view->dateTill = $filter->till;
            } else {
                $this->view->dateFrom = $defFrom;
                $this->view->dateTill = $defTill;
            }
        }

        // on POST filter will be rebuilt
        if ($this->getRequest()->isPost() || !isset($params->filter)) {
            $filter         = new stdClass();
            $filter->userId = (int)$this->currentUser->ID;
            $filter->type   = isset($params->type) ? $params->type : 'paid';

            switch ($filter->type) {
                case 'unpaid':
                    $filter->typeId = OrderModel::UNPAID;
                    break;
                case 'paid':
                    $filter->typeId = OrderModel::PAID;
                    break;
                case 'deleted':
                    $filter->typeId = OrderModel::DELETED;
                    break;
                case 'canceled':
                    $filter->typeId = OrderModel::CANCELED;
                    break;
                default:
                    $filter->typeId = OrderModel::UNPAID;
                    break;
            }

            $filter->daterangeOn = isset($params->switch) && $params->switch == 'on' || !$this->getRequest()->isPost()
                ? TRUE : FALSE;

            if ($filter->daterangeOn) {
                $filter->from         = isset($params->from) ? $params->from : $defFrom;
                $filter->till         = isset($params->till) ? $params->till : $defTill;
                $this->view->dateFrom = $filter->from;
                $this->view->dateTill = $filter->till;
            } else {
                $this->view->dateFrom = $defFrom;
                $this->view->dateTill = $defTill;
            }

            $filter->keywords    = isset($params->keywords) && !empty($params->keywords)
                ? BHelpers::clearUserInput($params->keywords) : '';
            $filter->orderBy     = 'order_id';
            $filter->direct      = 'DESC';
            $filter->limitOffset = 0;
            $filter->limitRows   = $this->rowsOnPage;
        }

        $filter->page = isset($params->page) ? (int)$params->page : 1;

        $this->view->switch           = $filter->daterangeOn;
        $this->view->type             = $filter->type;
        $this->view->typeId           = $filter->typeId;
        $this->view->keywords         = stripslashes(htmlspecialchars_decode($filter->keywords, ENT_QUOTES));
        $this->view->filterFormAction = $this->baseUrl . '/orders/index/type/' . $filter->type . '/';

        return $filter;
    }

    /**
     * Encode filter object as string.
     *
     * @param stdClass $filter
     *
     * @return string
     */
    protected function _encodeFilter(stdClass $filter) {
        return base64_encode(serialize($filter));
    }

    /**
     * Decode string to filter object.
     *
     * @param $string
     *
     * @return stdClass
     */
    protected function _decodeFilter($string) {
        return unserialize(base64_decode($string));
    }
}
