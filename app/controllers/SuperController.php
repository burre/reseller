<?php
/**
 * Project:   Atomic Reseller
 * File:      SuperController.php
 * Date:      23.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of SuperController
 *
 * @uses      BController
 * @author    Victor Burak <vb@atompark.com>
 *
 */
class SuperController extends BController
{
    /**
     * @var $app BApplication
     */
    public $app = NULL;
    /**
     * @var $front BFrontController
     */
    public $front = NULL;

    public $baseUrl = NULL;
    public $routerBase = NULL;
    public $currentUser = NULL;

    /**
     * This method may be omitted.
     * But in order to use your own init method be sure to run the parent's init method first.
     */
    public function init() {
        parent::init(); // important!

        $this->app   = BApplication::getInstance();
        $this->front = BFrontController::getInstance();

        $this->app->getLayout()->setDebugMode();
        $this->view->setDebugMode();

        $this->baseUrl = $this->getRequest()->protocol();
        $this->baseUrl .= $this->getRequest()->host();
        $this->baseUrl .= $this->app->getRegistry()->get('routes.base');
        $this->view->baseUrl = $this->baseUrl;

        $this->routerBase       = $this->app->getRegistry()->get('routes.base');
        $this->view->routerBase = $this->routerBase;

        $this->view->url = $this->app->getRequest()->url();

        $this->currentUser = $this->app->getRegistry()->get('user');


        /** @TODO Navigation menu with active item.
         *        May be implement it as a FrontController plugin?
         */
        //BFrontController::getInstance()->getResponse()->appendBody('navbar', 'Navigation');


    }

    /**
     * This method may be omitted.
     * But in order to use your own init method be sure to run the parent's init method first.
     */
    public function preDispatch() {
        parent::preDispatch(); // important!
    }

    /**
     * This method may be omitted.
     * But in order to use your own init method be sure to run the parent's init method first.
     */
    public function postDispatch() {
        parent::postDispatch(); // important!
    }

}

