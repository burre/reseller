<?php
/**
 * Project:   Application
 * File:      AdminIndexController.php
 * Date:      20.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BController
 *
 * @uses      BController
 * @author    Victor Burak <vb@atompark.com>
 */
class IndexController extends SuperController
{
    public function indexAction() {
        //BApplication::getInstance()->disableLayout();
        //$this->disableView();

    }

    public function aboutAction() {
    }

}
