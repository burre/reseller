<?php
/**
 * Project:   ${project.name}
 * File:      AdminIndexController.php
 * Date:      23.07.12
 *
 * @package   ${package}
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of IndexController
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class AdminIndexController extends BController
{
    public function indexAction() {
        pr('ADMIN CONTROLLER / INDEX ACTION');
    }

}
