<?php

/**
 * Project:   BTools
 * File:      BDBMapperAbstractct.php
 * Date:      20.11.2011
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Data mapper abstract class.
 *
 * @uses      BMapperInterface
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
abstract class BDBMapperAbstract implements BMapperInterface
{

    /**
     * BDbTable object.
     *
     * @var BDbTable
     */
    private $_db_table;

    /**
     * Mapper data.
     *
     * @var stdClass
     */
    private $_mapper_data;

    /**
     *Class constructor.
     *
     */
    abstract public function __construct();

    /**
     * Overloading for quick field access.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name) {
        if ($this->getDbTable()->hasField($name)) {
            return $this->_mapper_data->$name;
        } else {
            return NULL;
        }
    }

    /**
     * Overloading for quick field updating.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($name, $value) {
        if ($this->getDbTable()->hasField($name)) {
            $this->_mapper_data->$name = $value;
        }
    }

    /**
     * Initialise mapper data object.
     * All data previously loaded into mapper will be cleared.
     *
     * @return \BDBMapperAbstract
     */
    public function initMapperData() {
        $this->_mapper_data = new stdClass();

        $fields = $this->getDbTable()->getFieldList();
        foreach ($fields as $field) {
            $this->_mapper_data->$field = NULL;
        }

        return $this;
    }

    /**
     * Get the BDbTable object.
     *
     * @return BDbTable
     */
    public function getDbTable() {
        return $this->_db_table;
    }

    /**
     * Set the BDbTable object.
     *
     * @param BDbTable $db_table
     *
     * @return BDBMapperAbstract
     */
    protected function setDbTable(BDbTable $db_table) {
        $this->_db_table = $db_table;
        return $this;
    }

    /**
     * Find by one specified field.
     *
     * @param $fieldName
     * @param $value
     *
     * @return bool
     */
    public function findOneBy($fieldName, $value) {
        $retval = FALSE;
        $this->initMapperData();

        if ($this->getDbTable()->hasField($fieldName)) {
            $quote = $this->getDbTable()->needQuotes($fieldName) ? '\'' : '';
            $where = "`{$fieldName}` = {$quote}{$value}{$quote}";

            if ($result = $this->getDbTable()->select($where)) {
                $this->setMapperData($result[0]);
                $retval = TRUE;
            }
        }

        return $retval;
    }

    /**
     * Find by specified field.
     *
     * @param $fieldName
     * @param $value
     *
     * @return array Rowset
     */
    public function findBy($fieldName, $value) {
        if ($this->getDbTable()->hasField($fieldName)) {
            $quote = $this->getDbTable()->needQuotes($fieldName) ? '\'' : '';
            $where = "`{$fieldName}` = {$quote}{$value}{$quote}";

            return $this->getDbTable()->select($where);
        }
    }

    /**
     * Load model data.
     *
     * @TODO UGLY METHOD
     *
     * @param string $where
     *
     * @throws Exception
     * @return \BDBMapperAbstract
     */
    public function load($where = '') {
        if (!$where) {
            $idField = $this->getDbTable()->getPrimaryKey();
            if ($this->$idField) {
                if ($result = $this->getDbTable()->findByPrimaryKey($this->$idField)) {
                    $this->setMapperData($result[0]);
                } else {
                    $this->initMapperData();
                }
            } else {
                throw new Exception('Error: No primary key presented for select data!');
            }
        } else {

        }

        return $this;
    }

    public function create() {
        if ($new_id = $this->getDbTable()->insert((array)$this->_mapper_data))
            $this->id = $new_id;

        return $this;
    }

    public function delete() {
        if ($this->getDbTable()->delete($this->id)) {
            $this->initMapperData();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save() {
        if ($this->getDbTable()->update((array)$this->_mapper_data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Set the mapper's data from associative array (e.g query result).
     *
     * @param array $array
     *
     * @return BMapperAbstract
     */
    protected function setMapperData($array) {
        $this->_mapper_data = (object)$array;
        return $this;
    }


    /**
     * Returns the mapper's data as array (default) or
     * if $mode is TRUE - as stdObject.
     *
     * @param boolean $mode
     *
     * @return mixed array | stdObject
     */
    public function getMapperData($mode = FALSE) {
        return $mode ? $this->_mapper_data : (array)$this->_mapper_data;
    }

}