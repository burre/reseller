<?php

/**
 * Project:   BTools
 * File:      BFormBase.php 
 * Date:      25.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * BFormBase - base form class
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
class BFormBase {

    /**
     * Form template.
     * 
     * @var BTemplate
     */
    private $_tpl;

    /**
     * Form fields data.
     * 
     * @var array
     */
    protected $_formData = array();

    /**
     * Required fields list.
     * 
     * @var array
     */
    private $_requiredFields = array();

    /**
     * Forma validator.
     * 
     * @var BFormValidatorInterface
     */
    private $_validator;

    /**
     * Error messages for each field.
     * 
     * @var array 
     */
    private $_errMessages = array();

    public function __construct() {
        
    }

    /**
     * Set form template object.
     * 
     * @param BTemplate $tpl
     * @return BFormBase 
     */
    public function setTemplate(BTemplate $tpl) {
        $this->_tpl = $tpl;
        return $this;
    }

    /**
     * Get form template object.
     * 
     * @return BTemplate 
     */
    public function getTemplate() {
        return $this->_tpl;
    }

    /**
     * Set form data array.
     * 
     * @param array $formData
     * @return BFormBase 
     */
    public function setFormData(array $formData) {
        foreach ($formData as $key => $value) {
            $this->_formData[$key] = $value;
        }
        return $this;
    }

    /**
     * Get form data array.
     * 
     * @return array 
     */
    public function getFormData() {
        return $this->_formData;
    }

    /**
     * Clear all form data (sets empty array).
     * 
     * @return BFormBase 
     */
    public function clearFormData() {
        $this->_formData = array();
        return $this;
    }

    /**
     * Set list of required fields.
     * 
     * @param array $requiredFields
     * @return BFormBase 
     */
    public function setRequiredFields(array $requiredFields) {
        $this->_requiredFields = $requiredFields;
        return $this;
    }

    /**
     * Get list of required fields.
     * 
     * @return array
     */
    public function getRequiredFields() {
        return $this->_requiredFields;
    }

    /**
     * Set validator object for this form.
     * 
     * @param BFormValidatorInterface $validator
     * @return BFormBase 
     */
    public function setValidator(BFormValidatorInterface $validator) {
        $this->_validator = $validator;
        return $this;
    }

    /**
     * Get current validator of this form.
     * 
     * @return BFormValidatorInterface
     */
    public function getValidator() {
        return $this->_validator;
    }

    /**
     * Returns an array with error messages.
     * 
     * @return array
     */
    public function getErrMessages() {
        return $this->_errMessages;
    }

    /**
     * Add error message to the array.
     * 
     * @param string $key
     * @param string $message
     * @return void
     */
    public function addErrMessage($key, $message) {
        $this->_errMessages[$key] = $message;
    }

    /**
     * Prepare error messages and put them into the specified template.
     * 
     * @return BFormBase
     */
    protected function _prepareErrMessges(BTemplate $tpl) {
        $msgPrefix = '<span id="%ID%" class="error">';
        $msgSuffix = '</span>';

        foreach ($this->_errMessages as $name => $value) {
            $msgText = '';
            $msgId = $name . '_error';
            $msgPrefix = str_replace('%ID%', $msgId, $msgPrefix);
            $msgText = $msgPrefix . $value . $msgSuffix;
            $tpl->set($msgId, $msgText);
        }

        return $this;
    }

}
