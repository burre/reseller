<?php
/**
 * Project:   BTools
 * File:      BController.php
 * Date:      20.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BController
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 *
 * Methods implemented by BObject.
 * @method BController  setRequest() setRequest(BRequest $request) Set the last request object.
 * @method BRequest     getRequest() Get the last request object.
 * @method BController  setRoute(BRoute $route) Set the actual processed route.
 * @method BRoute       getRoute() Get the actual route.
 * @method BController  setView(BView $view)
 * @method BView        getView()
 *
 * @property BRequest   request
 * @property BObject    route
 * @property BView      view
 */
class BController extends BObject
{
    /**
     * @var BView
     */
    protected $_view = NULL;

    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->setStructure(
            array('request', 'route', 'view'),
            array('viewEnabled', 'dispatched'),
            self::BO_STRICT_STRUCTURE
        );
        $this->enableView();
    }

    /**
     * Initialize the controller.
     *
     * @return BController
     */
    protected function init() {
        $module       = $this->getRoute()->getModule();
        $controller   = $this->getRoute()->getController();
        $action       = $this->getRoute()->getAction();
        $viewBasePath = ($module == 'app') ? BT_APP_PATH : BT_MODULES_PATH . DS . $module . DS;
        $viewBasePath .= 'views' . DS . $controller . DS;
        $viewFile = $action . '.phtml';

        $view = new BView();
        $view->setViewPath($viewBasePath);
        $view->setViewFile($viewFile);
        $this->setView($view);

        return $this;
    }

    /**
     * Dispatch specified action.
     *
     *
     * @param string        $action
     * @param null|stdClass $params
     *
     * @return string
     */
    public function dispatch($action, stdClass $params = NULL) {
        if ($params === NULL) {
            $params = $this->getRoute()->getParams();
        }

        $response = '';

        $this->preDispatch();

        if (!$this->isDispatched()) {
            $response = $this->$action($params);

            $this->postDispatch();
            $this->setDispatched();

            if ($this->viewEnabled()) {
                $response = $this->getView()->parse();
            }
        }

        return $response;
    }

    /**
     * Enable using of view.
     *
     * @return BController
     */
    public function enableView() {
        $this->setOption('viewEnabled', TRUE);
        return $this;
    }

    /**
     * Disable using of view.
     *
     * @return BController
     */
    public function disableView() {
        $this->setOption('viewEnabled', FALSE);
        return $this;
    }

    /**
     * Check whether the view enabled.
     *
     * @return bool
     */
    public function viewEnabled() {
        return $this->getOption('viewEnabled');
    }

    /**
     * Pre-dispatch action.
     */
    public function preDispatch() {
    }

    /**
     * Post-dispatch action.
     */
    public function postDispatch() {
    }

    /**
     * Checks the controller status.
     *
     * @return bool
     */
    public function isDispatched() {
        return $this->getOption('dispatched');
    }

    /**
     * Set the controller status.
     *
     * @param bool $status
     *
     * @return BRoute
     */
    public function setDispatched($status = TRUE) {
        $this->setOption('dispatched', $status);
        return $this;
    }

    /**
     * Redirect 302.
     *
     * @param $url
     */
    public function redirect($url) {
        BFrontController::getInstance()->redirect($url);
    }

    /**
     * Transfer control into specific controller.
     * Via $params variable is possible to transfer params of current route or some extra.
     *
     * @param string  $module
     * @param string  $controller
     * @param string  $action
     * @param BObject $params
     *
     * @return void
     */
    public function forward($module, $controller, $action, BObject $params = NULL) {
        BFrontController::getInstance()->forward($module, $controller, $action, $params);
    }

}
