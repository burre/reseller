<?php

/**
 * Project:   BTools
 * File:      BDatabaseBase.php
 * Date:      17.04.2012
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * This class implements base database functionality.
 * It's used for extending into concrete classes
 * (e.g. BDatabaseS, BDatabaseNS).
 *
 * @uses      mysqli
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BDatabaseBase extends mysqli implements BDatabaseInterface
{

    /**
     * Object signature.
     *
     * @var string
     */
    protected $_info = '';

    /**
     * Connection resource.
     *
     * @var object
     */
    protected $_connection;

    /**
     * Credentials for database access.
     *
     * @var stdClass
     */
    protected $_credentials;

    /**
     * Query info.
     *
     * @var stdClass
     * @property stdClass->sql
     * @property stdClass->sql_hashe
     * @property stdClass->start_time
     * @property stdClass->end_time
     * @property stdClass->spent_time
     */
    protected $_query;

    /**
     * Query result.
     *
     * @var mysqli_result
     */
    protected $_result;

    /**
     * Last insert id.
     *
     * @var integer
     */
    protected static $_insert_id;

    /**
     * Summary query counter.
     *
     * @staticvar integer
     */
    protected static $_count = 0;

    /**
     * Summary cached query counter.
     *
     * @staticvar integer
     */
    protected static $_cached_count = 0;

    /**
     * Summary execution timer.
     *
     * @staticvar integer
     */
    protected static $_timer = 0;

    /**
     * Summary query list.
     *
     * @staticvar array
     */
    protected static $_query_list = array();

    /**
     * Cache for query results.
     *
     * @var array
     */
    protected static $_cache = array();

    /**
     * Close database connection.
     *
     * @return bool
     */
    public function close() {
        $this->_connection = NULL;

        return parent::close();
    }

    /**
     * Perform query and save info into $this.
     *
     * @param string $query
     * @throws Exception
     * @return BDatabaseBase
     */
    public function query($query) {
        $this->_query             = new stdClass();
        $this->_query->sql        = $query;
        $this->_query->sql_hash   = md5($this->_query->sql);
        $this->_query->start_time = microtime(TRUE);

        if ($this->_isCacheable($this->_query->sql) && $this->_isCached($this->_query->sql_hash)) {
            self::$_cached_count++;
        } else {
            self::$_count++;
            if ($this->_result = parent::query($this->_query->sql)) {
                $this->_query->end_time = microtime(TRUE);
                $this->_query->success  = TRUE;
                $this->_updateQueryList();
                $this->_updateTimer();
                self::$_insert_id = $this->insert_id;
            } else {
                $this->_query->success = FALSE;
                throw new Exception("Query performing error $this->errno: $this->error");
            }
        }
        return $this;
    }

    /**
     * Return result row as associative array.
     *
     * @return array
     */
    public function getRowAsArray() {
        if ($this->_isCacheable($this->_query->sql) && $this->_isCached($this->_query->sql_hash)) {
            $retval = $this->_cacheGet($this->_query->sql_hash);
        } else {
            if ($this->getLastResult() && $this->getAffectedRows()) {
                $retval = $this->getLastResult()->fetch_assoc();
                $this->freeLastResult();
                if ($this->_isCacheable($this->_query->sql))
                    $this->_cacheAdd($this->_query->sql_hash, $retval);
            } else {
                $retval = array();
            }
        }
        return $retval;
    }

    /**
     * Return result row as object.
     *
     * @return stdClass
     */
    public function getRowAsObject() {
        if ($this->_isCacheable($this->_query->sql) && $this->_isCached($this->_query->sql_hash)) {
            $retval = $this->_cacheGet($this->_query->sql_hash);
        } else {
            if ($this->getLastResult() && $this->getAffectedRows()) {
                $retval = $this->getLastResult()->fetch_object();
                $this->freeLastResult();
                if ($this->_isCacheable($this->_query->sql))
                    $this->_cacheAdd($this->_query->sql_hash, $retval);
            } else {
                $retval = new stdClass();
            }
        }
        return $retval;
    }

    /**
     * Return all result rows as array of associative arrays.
     *
     * @return array
     */
    public function getRowsetAsArray() {
        if ($this->_isCacheable($this->_query->sql) && $this->_isCached($this->_query->sql_hash)) {
            $retval = $this->_cacheGet($this->_query->sql_hash);
        } else {
            $retval = array();
            if ($this->getLastResult() && $this->getAffectedRows()) {
                // This method only for PHP >= 5.3
                //$retval = $this->getLastResult()->fetch_all(MYSQLI_ASSOC);
                while ($row = $this->getLastResult()->fetch_assoc()) {
                    $retval[] = $row;
                }
                $this->freeLastResult();
                if ($this->_isCacheable($this->_query->sql)) {
                    $this->_cacheAdd($this->_query->sql_hash, $retval);
                }
            }
        }
        return $retval;
    }

    /**
     * Escape string for use in a SQL queries.
     *
     * @param string $string
     * @return string
     */
    public function escape($string) {
        // @TODO: Here is error with escaping chars. Need to be fixed.
        return $this->real_escape_string($string);
    }

    /**
     * Get the affected rows counter for the last query.
     *
     * @return integer
     */
    public function getAffectedRows() {
        return $this->affected_rows;
    }

    /**
     * Get last insert id.
     *
     * @return integer
     */
    public function getInsertId() {
        return self::$_insert_id;
    }

    /**
     * Get the amount query counter.
     *
     * @return integer
     */
    public static function getCounter() {
        return self::$_count;
    }

    /**
     * Get the amount cached query counter.
     *
     * @return integer
     */
    public static function getCachedCounter() {
        return self::$_cached_count;
    }

    /**
     * Get the summary execution timer.
     *
     * @return integer
     */
    public static function getTimer() {
        return self::$_timer;
    }

    /**
     * Update the summary timer info.
     *
     * @return void
     */
    private function _updateTimer() {
        self::$_timer += $this->_query->spent_time;
    }

    /**
     * Update the query list info.
     *
     * @return void
     */
    private function _updateQueryList() {
        $this->_query->spent_time = $this->_query->end_time - $this->_query->start_time;
        self::$_query_list[]      = $this->_query;
    }

    /**
     * Get the query list.
     *
     * @return array
     */
    public static function getQueryList() {
        return self::$_query_list;
    }

    /**
     * Get the last result.
     *
     * @return mysqli_result
     */
    public function getLastResult() {
        return $this->_result;
    }

    /**
     * Free memory used by last result.
     *
     * @return void
     */
    public function freeLastResult() {
        $this->_result->free();
        $this->_result = NULL;
    }

    /**
     * Get debug adapter summary.
     *
     * @return array
     */
    public static function getDebugInfo() {
        return array(
            'query_list'     => self::getQueryList(),
            'counter'        => self::getCounter(),
            'cached_counter' => self::getCachedCounter(),
            'timer'          => self::getTimer(),
            'last_id'        => self::getInsertId(),
            'cache'          => self::$_cache,
        );
    }

    /**
     * Put value into cache.
     *
     * @param string       $key
     * @param mysql_result $val
     */
    private function _cacheAdd($key, $val) {
        self::$_cache[$key] = $val;
    }

    /**
     * Delete value from cache.
     *
     * @param string $key
     */
    private function _cacheDel($key) {
        if ($this->_isCached($key))
            unset(self::$_cache[$key]);
    }

    /**
     * Clear the cache.
     *
     * @return void
     */
    private function _cacheClear() {
        self::$_cache = array();
    }

    /**
     * Checks for cache element by key.
     *
     * @param string $key
     * @return bool
     */
    private function _isCached($key) {
        return array_key_exists($key, self::$_cache);
    }

    /**
     * Get value from cache.
     *
     * @param string $key
     * @return mysqli_result
     */
    private function _cacheGet($key) {
        return $this->_isCached($key) ? self::$_cache[$key] : NULL;
    }

    /**
     * Checks if query is cacheable.
     *
     * @param string $query
     * @return bool
     */
    private function _isCacheable($query) {
        $pattern = '/SELECT|DESCRIBE|SHOW|EXPLAIN/i';
        return preg_match($pattern, $query);
    }

    /**
     * Returns a character set object providing several
     * properties of the current active character set.
     * Also added info about current class.
     *
     * @return stdClass
     */
    public function getCharSet() {
        $charset       = $this->get_charset();
        $charset->info = 'Charset info of ' . $this->_info;
        return $charset;
    }

    /**
     * Sets the default character set to be used
     * when sending data from and to the database server.
     *
     * @param string $charset
     * @return boolean TRUE if succes or FALSE if failure.
     */
    public function setCharSet($charset) {
        $this->charset = $charset;
        return $this->set_charset($this->charset);
    }

}
