<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      BMail.php
 * Date:      20.03.2012
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BMail
 *
 * @uses      SMTP
 * @package   NAD
 * @author    Victor Burak <vb@atompark.com>
 */
class BMail extends SMTP {

    const BMAIL_DEFAULT_FROM = 'tech@atompark.com';

    /**
     * Credentials for SMTP server.
     * @staticvar stdClass
     */
    private $_credentials;

    private $_to = '';
    private $_cc = '';
    private $_bcc = '';
    private $_from = '';
    private $_return_path = '';
    private $_reply_to = '';
    private $_content_type = 'text/html; charset=utf-8';
    private $_subject = '';
    private $_headers = '';
    private $_body = '';
    private $_myHost = '';

    /**
     * BMail class constructor.
     *
     * @param  array $credentials
     * @throws Exception
     */
    public function __construct($credentials) {
        if ($credentials && is_array($credentials)) {
            $this->_credentials = (object) $credentials;
        } else {
            throw new Exception('Error! No credentials provided for a SMTP server access.');
        }
        $this->setMyHost();
    }

    /**
     * Sets sender address.
     *
     * @param string $from
     * @return BMail
     */
    public function setFrom($from) {
        $this->_from = $from;
        return $this;
    }

    /**
     * Sets return path address.
     *
     * @param string $returnPath
     * @return BMail
     */
    public function setReturnPath($returnPath) {
        $this->_return_path = $returnPath;
        return $this;
    }

    /**
     * Sets reply-to address.
     *
     * @param string $replyTo
     * @return BMail
     */
    public function setReplyTo($replyTo) {
        $this->_reply_to = $replyTo;
        return $this;
    }

    /**
     * Sets content type.
     *
     * @param string $contentType
     * @return BMail
     */
    public function setContentType($contentType) {
        $this->_content_type = $contentType;
        return $this;
    }

    /**
     * Sets subject text.
     *
     * @param string $subject
     * @return BMail
     */
    public function setSubject($subject) {
        $this->_subject = $subject;
        return $this;
    }

    /**
     * Sets message body text.
     *
     * @param string $bodyText
     * @return BMail
     */
    public function setBodyText($bodyText) {
        $this->_body = $bodyText;
        return $this;
    }

    /**
     * Sets receiver address.
     *
     * @param string $to
     * @return BMail
     */
    public function addTo($to) {
        $this->_to = $to;
        return $this;
    }

    /**
     * Sets carbon copy address.
     *
     * @param string $cc
     * @return BMail
     */
    public function addCc($cc) {
        $this->_cc = $cc;
        return $this;
    }

    /**
     * Sets blind carbon copy address
     *
     * @param string $bcc
     * @return BMail
     */
    public function addBcc($bcc) {
        $this->_bcc = $bcc;
        return $this;
    }

    /**
     * Adds aby header to message.
     *
     * @param string $header
     * @return BMail
     */
    public function addHeader($header) {
        $this->_headers .= $header . "\r\n";
        return $this;
    }

    /**
     * Sets sender host name.
     *
     * @param string $myHost
     * @return BMail
     */
    public function setMyHost($myHost = '') {
        $this->_myHost = empty($myHost) && isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $myHost;
        return $this;
    }

    /**
     * Send email message.
     *
     * @return boolean
     */
    public function send() {
        $this->_checkFrom();
        $this->_prepareHeaders();
        if ($this->Connect($this->_credentials->smtpHost, $this->_credentials->smtpPort)) {
            if ($this->AuthHello($this->_myHost, $this->_credentials->smtpUser, $this->_credentials->smtpPass)) {
                $this->MailFrom($this->_from);
                $this->Recipient($this->_to);
                $this->Data($this->_headers . $this->_body);
                $this->Quit();
            } else
                return $this->error;
        } else
            return $this->error;
        $this->Close();

        return TRUE;
    }

    /**
     * Prepare headers before mail sending.
     *
     * @return BMail
     */
    private function _prepareHeaders() {
        if ($this->_headers) {
            $this->_headers .= "\r\n";
        }
        if ($this->_to) {
            $this->addHeader("To: $this->_to");
        } else {
            throw new Exception('BMail error: Uncnown recipient!');
        }
        if ($this->_subject) {
            $this->addHeader("Subject: $this->_subject");
        }
        if ($this->_cc) {
            $this->addHeader("CC: $this->_cc");
        }
        if ($this->_bcc) {
            $this->addHeader("BCC: $this->_bcc");
        }
        if ($this->_reply_to) {
            $this->addHeader("Reply-To: <$this->_reply_to>");
        }
        if ($this->_return_path) {
            $this->addHeader("Return-Path: <$this->_return_path>");
        } elseif ($this->_from) {
            $this->addHeader("Return-Path: <$this->_from>");
        }
        if ($this->_content_type) {
            $this->addHeader("Content-Type: $this->_content_type");
        }
        return $this;
    }

    /**
     * Check for empty `from` property.
     *
     * @return void
     */
    private function _checkFrom() {
        if (empty($this->_from)) {
            $this->_from = self::BMAIL_DEFAULT_FROM;
        }
    }

    /**
     * Clear object variables.
     *
     * @return BMail
     */
    public function clear() {
        $this->_to = '';
        $this->_cc = '';
        $this->_bcc = '';
        $this->_from = '';
        $this->_return_path = '';
        $this->_reply_to = '';
        $this->_content_type = 'text/html; charset=utf-8';
        $this->_subject = '';
        $this->_headers = '';
        $this->_body = '';
        $this->_myHost = '';

        return $this;
    }
}
