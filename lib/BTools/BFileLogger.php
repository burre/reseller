<?php

/**
 * Project:   BTools
 * File:      BFileLogger.php 
 * Date:      26.03.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * BFileLogger class.
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BFileLogger {

    /**
     * Name of log file.
     * @var string 
     */
    private $_logFileName;
    
    /**
     * File opening mode:<br />
     * 'a' (append) - for the exists file;
     * 'c' (create) - for the new file;
     * or any allowed mode, forced while creating object.
     * @var string 
     */
    private $_logFileMode;
    
    /**
     * Handler of opened log file.
     * @var resource 
     */
    private $_logFileHandler;

    /**
     * Class constructor.
     * @param string $logFileName
     * @param string $logFileMode
     * @throws Exception 
     */
    public function __construct($logFileName, $logFileMode = '') {
        if (!empty($logFileName)) {
            $this->_logFileName = $logFileName;
            $this->_logFileMode = !empty($logFileMode) ? $logFileMode : 
                (file_exists($logFileName) ? 'a' : 'c');
            $this->open($this->_logFileMode);
        } else {
            throw new Exception('BFileLogger: No file name specified for logging.');
        }
    }
    
    /**
     * Open log file.
     * @param string $logFileMode
     * @return BFileLogger
     * @throws Exception 
     */
    public function open($logFileMode = '') {
        if (!empty($logFileMode)) {
            $this->_logFileMode = $logFileMode;
        }
        if (!$this->_logFileHandler = @fopen($this->_logFileName, $this->_logFileMode)) {
            throw new Exception('BFileLogger: File opening error.');
        }
        return $this;
    }

    /**
     * Write line to log file.
     * @param string $row
     * @throws Exception 
     * @return BFileLogger
     */
    public function write($row) {
        $date = date('d.m.Y H:i:s');
        $row  = "$date : $row \n";
        if (@fwrite($this->_logFileHandler, $row) === FALSE) {
            throw new Exception('BFileLogger: Can\'t write the log file.');
        }
        return $this;
    }

    /**
     * Close log file handler.
     * @return void 
     */
    public function close() {
        @fclose($this->_logFileHandler);
    }

}
