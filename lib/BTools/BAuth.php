<?php
/**
 * Project:   BTools
 * File:      BAuth.php
 * Date:      22.08.12
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BAuth
 *
 * @uses      parent_class_name
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 *
 * @method BAuth  setIdentityModel(string $identityModel) Set the class name of identity model.
 * @method string getIdentityModel()                      Get the class name of identity model.
 * @method BAuth  setIdentityField(string $loginField)
 * @method string getIdentityField()
 * @method BAuth  setPasswordField(string $passwordField)
 * @method string getPasswordField()
 * @method BAuth  setIdentityObject($identity)
 * @method string getIdentityObject()
 *
 * @property string identityModel
 * @property string identityField
 * @property string passwordModel
 * @property string identityObject
 */
class BAuth extends BObject
{
    /**
     * Class constructor.
     *
     * @param string $identityModel
     * @param string $loginField
     * @param string $passwordField
     */
    public function __construct($identityModel, $loginField, $passwordField) {
        parent::__construct();

        $this->setStructure(
            array('identityModel', 'identityField', 'passwordField', 'identityObject'),
            array(),
            self::BO_STRICT_STRUCTURE
        );

        $this->setIdentityModel($identityModel);
        $this->setIdentityField($loginField);
        $this->setPasswordField($passwordField);

        $this->init();
    }

    /**
     * Initialize object.
     * Used for children classes.
     */
    public function init() {

    }

    /**
     * Authenticate user by login and password.
     *
     * @param string $login
     * @param string $password
     *
     * @return bool
     */
    public function authenticate($login, $password) {
        $retval = FALSE;

        if ($this->getIdentity($login)) {
            $passwordField = $this->getPasswordField();

            if ($this->identityObject->$passwordField == $password) {
                $retval = TRUE;
            }
        }

        return $retval;
    }

    /**
     * Get identity model by login.
     *
     * @param string $login
     * @param bool   $forceReload Force reload the model data from database.
     *
     * @return BUserInterface
     */
    public function getIdentity($login, $forceReload = FALSE) {
        if ($this->identityObject === NULL || $forceReload) {
            $this->identityObject = new $this->identityModel();

            if (!$this->identityObject->findOneBy($this->identityField, $login)) {
                $this->identityObject = NULL;
            }
        }

        return $this->identityObject;
    }

}
