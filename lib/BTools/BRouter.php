<?php
/**
 * Project:   BTools
 * File:      BRouter.php
 * Date:      18.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BRouter
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BRouter
{
    const ROUTER_FORCE_REBUILD = TRUE;

    /**
     * @var BRequest
     */
    private $_request;
    private $_staticsRoutes = array();
    private $_base;

    /**
     * @var BObject
     */
    private $_route;

    /**
     * Class constructor.
     */
    public function __construct(BRequest $request, $base = '') {
        $this->init($request, $base);
    }

    /**
     * Initialize router.
     *
     * @param BRequest $request
     * @param string   $base
     *
     * @return \BRouter
     */
    public function init(BRequest $request, $base = '') {
        $this->_request = $request;
        $this->_base    = $base;

        $this->_route = new BRoute();

        return $this;
    }

    /**
     * Get the last request URI without the router base and the query string.
     * In fact query string will be ignored, any GET data
     * will parsed from URI (as a "key/value parameters).
     *
     * @return string
     */
    public function getUri() {
        $uri = $this->_request->uri();

        if ($this->_base) {
            $uri = str_replace($this->_base, '', $uri);
        }

        if ($this->_request->queryString()) {
            $uri = str_replace('?' . $this->_request->queryString(), '', $uri);
        }

        return $uri;
    }

    /**
     * Get route for last request.
     * Common parts of request URI are next:
     * [protocol]://domain.com/base/module/controller/action/param1/val1/param2/val2/...
     * - module     - default is 'app'. May be omitted.
     * - controller - default is 'index'. May be omitted.
     * - action     - default is 'index'. May be omitted.
     * Any pairs of parameters are allowed after all three previous parts.
     *
     * @param bool $forceRebuild
     *
     * @return BRoute
     */
    public function getRoute($forceRebuild = FALSE) {
        if (!$forceRebuild && !$this->_route->isEmpty()) {
            return $this->_route;
        } else {
            $segments = explode('/', $this->getUri());

            $this->_makeRoute($segments);

            return $this->_route;
        }
    }

    protected function _makeRoute(array $segments) {
        foreach ($segments as $key => $val) {
            if (empty($val)) {
                unset($segments[$key]);
            }
        }

        $segments = array_values($segments);
        $count    = count($segments);
        $params   = array();

        switch ($count) {
            case 0: // only '/'
                $module     = 'app';
                $controller = 'index';
                $action     = 'index';
                break;
            case 1: // only controller
                $module     = 'app';
                $controller = 'index';
                $action     = $segments[0];
                break;
            case 2: // only controller and action
                $module     = 'app';
                $controller = $segments[0];
                $action     = $segments[1];
                break;
            default: // 3 and more segments
                /**
                 * If there is only default module - all segments after 2 first
                 * will be processed as pairs of parameters, otherwise parameter list starts after 3 segments.
                 */
                $modules = BFrontController::getInstance()->getRegistry()->get('modules');

                if (count($modules) > 1) {
                    $module     = $segments[0];
                    $controller = $segments[1];
                    $action     = $segments[2];
                    $toSlice    = 3;
                } else {
                    $module     = 'app';
                    $controller = $segments[0];
                    $action     = $segments[1];
                    $toSlice    = 2;
                }

                $segments = array_slice($segments, $toSlice);
                $count    = count($segments);

                for ($i = 0; $i < $count; $i++) {
                    $params[$segments[$i]] = isset($segments[++$i]) ? $segments[$i] : '';
                }

                break;
        }

        $this->_route->setModule($module);
        $this->_route->setController($controller);
        $this->_route->setAction($action);
        $this->_route->params->setData($params);
    }

}
