<?php
/**
 * Project:   BTools
 * File:      BSingleton.php
 * Date:      16.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BSingleton
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BSingleton
{
    /**
     * @var BSingleton
     */
    protected static $_instance = NULL;

    /**
     * Disabled by access level
     *
     * @return \BSingleton
     */
    protected function __construct() {
        self::setInstance($this);
    }

    /**
     * Set the singleton instance.
     *
     * @param BSingleton $instance
     *
     * @throws Exception
     * @return BSingleton
     */
    final static private function setInstance($instance) {
        if ($instance instanceof self) {
            self::$_instance = $instance;
        } else {
            throw new Exception('First parameter for method `' . __METHOD__ . '` should be instance of `' . __CLASS__ . '`');
        }
        return self::$_instance;
    }

    /**
     * Get the singleton instance.
     *
     * Variant for PHP >= 5.3:
     * self::$instance = new static;
     *
     * @return BSingleton
     */
    static public function getInstance() {
        return isset(self::$_instance)
            ? self::$_instance
            : self::$_instance = new self();
    }

    /**
     * Disabled by access level
     */
    protected function __wakeup() {

    }

    /**
     * Disabled by access level
     */
    protected function __clone() {

    }

}
