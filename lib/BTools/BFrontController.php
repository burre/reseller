<?php
/**
 * Project:   BTools
 * File:      BFrontController.php
 * Date:      19.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BFrontController (Singleton)
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BFrontController
{

    /**
     * @var BFrontControllerPluginBrocker
     */
    protected $_plugins = NULL;

    /**
     * @var BRegistry
     */
    protected $_registry = NULL;

    /**
     * @var BRequest
     */
    protected $_request = NULL;

    /**
     * @var BRouter
     */
    protected $_router = NULL;

    /**
     * @var BResponse
     */
    protected $_response = NULL;

    //=== Singleton part

    /**
     * @var BApplication
     */
    protected static $_instance = NULL;

    /**
     * Disabled by access level
     *
     * @return \BFrontController
     */
    protected function __construct() {
        self::setInstance($this);
    }

    /**
     * Set the singleton instance.
     *
     * @param \BFrontController $instance
     *
     * @throws Exception
     * @return \BFrontController
     */
    final static private function setInstance(BFrontController $instance) {
        if ($instance instanceof self) {
            self::$_instance = $instance;
        } else {
            throw new Exception('First parameter for method `' . __METHOD__ . '` should be instance of `' . __CLASS__ . '`');
        }
        return self::$_instance;
    }

    /**
     * Get the singleton instance.
     *
     * Variant for PHP >= 5.3:
     * self::$instance = new static;
     *
     * @return \BFrontController
     */
    static public function getInstance() {
        return isset(self::$_instance)
            ? self::$_instance
            : self::$_instance = new self();
    }

    /**
     * Disabled by access level
     */
    protected function __wakeup() {

    }

    /**
     * Disabled by access level
     */
    protected function __clone() {

    }

    //=== end of Singleton part

    /**
     *  Initialize the front controller.
     *
     * @param array $options Allowed keys is 'registry', 'request'
     *
     * @return BFrontController
     */
    public function init(array $options = array()) {

        if (isset($options['registry'])) {
            $this->setRegistry($options['registry']);
        }

        if (isset($options['request'])) {
            $this->setRequest($options['request']);
        }

        $this->setResponse(new BResponse());

        $this->_plugins = new BFrontControllerPluginBrocker();

        return $this;
    }

    /**
     * Dispatch the route.
     *
     * @param BRequest $request
     *
     * @throws Exception
     * @internal param \BRoute|null $route
     *
     * @return BResponse
     */
    public function dispatch(BRequest $request = NULL) {
        $this->_plugins->routeStartup();

        if (!$request) {
            $request = $this->getRequest();
        } else {
            if (!($request instanceof BRequest)) {
                throw new Exception('BFrontController: wrong route object submitted to dispatch()!');
            }
        }

        $this->setRouter(new BRouter($request, $this->getRegistry()->get('routes.base')));
        $route = $this->getRouter()->getRoute();

        $this->_plugins->routeShutdown();
        $this->_plugins->dispatchLoopStartup();

        do {
            if ($this->routeIsAllowed($route)) {
                $this->_plugins->preDispatch();
                $this->follow($route);
            } else {
                $this->getResponse()->setResponseCode('404');
                $request->setDispatched();
            }

            /**
             * @TODO Here must be the block for trapping exceptions or something like that.
             */
            $request->setDispatched();
            $this->_plugins->postDispatch();
        } while (!$request->isDispatched());

        $this->_plugins->dispatchLoopShutdown();

        return $this->getResponse();
    }

    /**
     * Follow the specified route.
     *
     * @param BRoute $route
     */
    public function follow(BRoute $route) {
        if (!$this->getRequest()->isDispatched()) {
            /**
             * @TODO Should to check the module before
             */
            $className  = $this->getControllerClassName($route->getController());
            $actionName = $this->getActionMethodName($route->getAction());

            /** @var $controller BController */
            $controller = new $className();
            $controller->setRequest($this->getRequest());
            $controller->setRoute($route);
            $controller->init();

            if ($content = $controller->dispatch($actionName)) {
                $this->getResponse()->appendBody('content', $content);
            } else {
                $this->getResponse()->appendBody('content', '');
            }
        }
    }

    /**
     * Get application router.
     *
     * @return BRouter
     */
    public function getRouter() {
        return $this->_router;
    }

    /**
     * Set application router.
     *
     * @param BRouter $router
     */
    public function setRouter(BRouter $router) {
        $this->_router = $router;
    }

    /**
     * Get application registry.
     *
     * @return BRegistry
     */
    public function getRegistry() {
        return $this->_registry;
    }

    /**
     * Set application registry.
     *
     * @param BRegistry $registry
     *
     * @return BFrontController
     */
    public function setRegistry(BRegistry $registry) {
        $this->_registry = $registry;
        return $this;
    }

    /**
     * Get last request object.
     *
     * @return BRequest
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * Set last request object.
     *
     * @param BRequest $request
     *
     * @return BFrontController
     */
    public function setRequest(BRequest $request) {
        $this->_request = $request;
        return $this;
    }

    /**
     * Get current response.
     *
     * @return BResponse
     */
    public function getResponse() {
        return $this->_response;
    }

    /**
     * Set response.
     *
     * @param BResponse $response
     */
    public function setResponse(BResponse $response) {
        $this->_response = $response;
    }

    /**
     * Retrieve the module directory path for specified route.
     *
     * @param BObject $route
     *
     * @return string
     */
    public function getModuleDir(BObject $route) {
        return $route->getModule() == 'app' ? BT_APP_PATH : BT_MODULES_PATH . $route->getModule . DS;
    }

    /**
     * Retrieve the controller file for specified route.
     *
     * @param BObject $route
     *
     * @return string
     */
    public function getControllerFile(BObject $route) {
        $path = $route->getModule() == 'app' ? BT_CONTROLLERS_PATH : BT_MODULES_PATH . 'controllers' . DS;
        $path .= ucfirst($route->getController()) . 'Controller.php';
        return $path;
    }

    /**
     * Retrieve the controller class name for specified route.
     *
     * @param $controllerName
     *
     * @return string
     */
    public function getControllerClassName($controllerName) {
        return ucfirst($controllerName) . 'Controller';
    }

    /**
     * Get full action method name (with suffix 'Action').<br/>
     * Dashes are processed as word delimiters, therefore route parts
     * like 'some-actions-name' will be converted to 'someActionsNameAction'
     *
     * @param $actionName
     *
     * @return string
     */
    private function getActionMethodName($actionName) {
        $pos = strpos($actionName, '-');

        if (FALSE !== $pos && $pos > 0) {
            $words      = explode('-', $actionName);
            $actionName = '';
            foreach ($words as $word) {
                $actionName .= $word;
            }
        }

        return lcfirst($actionName) . 'Action';
    }

    /**
     * Check whether the controller exists.
     *
     * @param BObject $route
     *
     * @return bool
     */
    public function controllerExists(BObject $route) {
        $file = $this->getControllerFile($route);
        return file_exists($file);
    }

    /**
     * Check the route whether is allowed or not.
     *
     * @param BObject $route
     *
     * @return bool
     */
    public function routeIsAllowed(BObject $route) {
        $retval = FALSE;

        if ($this->controllerExists($route)) {
            $className  = $this->getControllerClassName($route->getController());
            $methodName = $this->getActionMethodName($route->getAction());

            if (method_exists($className, $methodName)) {
                $retval = TRUE;
            }

        }

        return $retval;
    }

    /**
     * Register a plugin.
     *
     * @param BFrontControllerPluginAbstract $plugin
     *
     * @return \BFrontController
     */
    public function registerPlugin(BFrontControllerPluginAbstract $plugin) {
        $this->_plugins->registerPlugin($plugin);
        return $this;
    }

    /**
     * Unregister a plugin.
     *
     * @param string $plugin
     *
     * @return \BFrontController
     */
    public function unregisterPlugin($plugin) {
        $this->_plugins->unregisterPlugin($plugin);
        return $this;
    }

    /**
     * Redirect 302.
     *
     * @param $url
     */
    public function redirect($url) {
        header("Location: {$url}");
    }

    /**
     * Transfer control into specific controller.
     * Via $params variable is possible to transfer params of current route or some extra.
     *
     * @param string  $module
     * @param string  $controller
     * @param string  $action
     * @param BObject $params
     *
     * @return void
     */
    public function forward($module, $controller, $action, BObject $params = NULL) {
        $route             = new BRoute();
        $route->module     = $module;
        $route->controller = $controller;
        $route->action     = $action;
        $route->params     = $params == NULL ? new stdClass() : $params;

        $this->follow($route);
    }
}
