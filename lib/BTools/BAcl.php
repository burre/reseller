<?php
/**
 * Project:   BTools
 * File:      BAcl.php
 * Date:      26.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BAcl
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
define('BACL_NONE', 1 << 0);
define('BACL_READ', 1 << 1);
define('BACL_WRITE', 1 << 2);
define('BACL_INSERT', 1 << 3);
define('BACL_DELETE', 1 << 4);
define('BACL_READ_WRITE', BACL_READ | BACL_WRITE);
define('BACL_READ_WRITE_INSERT', BACL_READ | BACL_WRITE | BACL_INSERT);
define('BACL_READ_WRITE_DELETE', BACL_READ | BACL_WRITE | BACL_DELETE);
define('BACL_ALL', BACL_READ | BACL_WRITE | BACL_INSERT | BACL_DELETE);

class BAcl
{
    const READ   = BACL_READ;
    const WRITE  = BACL_WRITE;
    const INSERT = BACL_INSERT;
    const DELETE = BACL_DELETE;
    const ALL    = BACL_ALL;

    /**
     * @var array
     */
    protected $_roles = array();

    /**
     * @var array
     */
    protected $_resources = array();

    /**
     * Class constructor.
     *
     * @param $configFile
     *
     * @throws Exception
     */
    public function __construct($configFile) {
        $data = BHelpers::parseConfig($configFile);

        if (!isset($data['roles'])) {
            throw new Exception('BAcl: No roles in config file!');
        }

        $this->_roles = $data['roles'];
        unset($data['roles']);

        foreach ($data['resources'] as $key => $val) {
            $data['resources'][$key] = explode(',', $val);
        }

        $this->_resources = $data['resources'];

        //s($this);

        //s(BACL_NONE, BACL_READ, BACL_WRITE, BACL_INSERT, BACL_DELETE,
        //    BACL_READ_WRITE, BACL_READ_WRITE_INSERT, BACL_READ_WRITE_DELETE, BACL_ALL);

    }

    /**
     * Check specified resource with control list.
     * Resource should be in full format, i.e. module/controller/action.
     * Leading and trailing slashes will be stripped.
     *
     * @param $role
     * @param $resource
     *
     * @return bool
     */
    public function checkResource($role, $resource) {
        $retval           = FALSE;
        $allowedResources = $this->getRolePermissions($role);
        $resource         = preg_replace('/^\/|\/$/', '', $resource); // strip leading and trailing slashes

        //s($resource, $allowedResources);

        if (in_array($resource, $allowedResources)) {
            // full resource - module/controller/action
            $retval = TRUE;
        } else {
            // partial resource - module/controller or module
            $segments = explode('/', $resource);

            foreach ($allowedResources as $item) {
                $s = explode('/', $item);
                $c = count($s);

                if ($c == 2 && $segments[0] == $s[0] && $segments[1] == $s[1]) {
                    $retval = TRUE;
                } elseif ($c == 1 && $segments[0] == $s[0]) {
                    $retval = TRUE;
                }

                if ($retval) {
                    break;
                }
            }
        }

        return $retval;
    }

    /**
     * Get resources list of specified role.
     *
     * @param $role
     *
     * @return array
     */
    public function getRolePermissions($role) {
        $retval = array();

        if ($this->hasRole($role)) {
            foreach ($this->_resources as $key => $val) {
                if (in_array($role, $val)) {
                    $retval[] = $key;
                }
            }
        }

        return $retval;
    }

    /**
     * Check if ACL has a specified role.
     *
     * @param $role
     *
     * @return bool
     */
    public function hasRole($role) {
        return array_key_exists($role, $this->_roles);
    }

}
