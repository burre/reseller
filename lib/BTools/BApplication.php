<?php
/**
 * Project:   BTools
 * File:      BApplication.php
 * Date:      16.07.12
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BApplication (Singleton)
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BApplication
{

    /**
     * Application config file.
     *
     * @var string
     */
    protected $_config_file;

    /**
     * Application registy.
     *
     * @var BRegistry
     */
    protected $_registry;

    /**
     * @var BRequest
     */
    protected $_request;

    /**
     * @var BLayout
     */
    protected $_layout;

    /**
     * @var bool
     */
    protected $_layoutEnabled = TRUE;

    /**
     * @var BFrontController
     */
    protected $_frontController;

    /**
     * @var string
     */
    protected $_baseUrl = '';

    //=== Singleton part

    /**
     * @var BApplication
     */
    protected static $_instance = NULL;

    /**
     * Disabled by access level
     *
     * @return \BApplication
     */
    protected function __construct() {
        self::setInstance($this);
    }

    /**
     * Set the singleton instance.
     *
     * @param BApplication $instance
     *
     * @throws Exception
     * @return BApplication
     */
    final static private function setInstance(BApplication $instance) {
        if ($instance instanceof self) {
            self::$_instance = $instance;
        } else {
            throw new Exception('First parameter for method `' . __METHOD__ . '` should be instance of `' . __CLASS__ . '`');
        }
        return self::$_instance;
    }

    /**
     * Get the singleton instance.
     *
     * Variant for PHP >= 5.3:
     * self::$instance = new static;
     *
     * @return BSingleton
     */
    static public function getInstance() {
        return isset(self::$_instance)
            ? self::$_instance
            : self::$_instance = new self();
    }

    /**
     * Disabled by access level
     */
    protected function __wakeup() {

    }

    /**
     * Disabled by access level
     */
    protected function __clone() {

    }

    //=== end of Singleton part

    /**
     * Initialize the application.
     *
     * @param string $configFile
     *
     * @return BApplication
     */
    public function init($configFile = '') {
        $this->setRequest(new BRequest());
        $this->setRegistry(new BRegistry());

        // set application's variables from config file
        $this->_config_file = $configFile;
        $this->getRegistry()->addArray(BHelpers::parseConfig($this->_config_file));

        $layout = new BLayout();
        $layout->setViewPath(BT_LAYOUTS_PATH . 'default' . DS);
        $layout->setViewFile('index.phtml');
        $this->setLayout($layout);

        $this->registerModule('app', TRUE, BT_APP_PATH); // the default module

        $this->setFrontController(BFrontController::getInstance());
        $this->getFrontController()
            ->setRegistry($this->getRegistry())
            ->setRequest($this->getRequest())
            ->init();

        $this->_baseUrl = $this->getRequest()->protocol();
        $this->_baseUrl .= $this->getRequest()->host();
        $this->_baseUrl .= $this->getRegistry()->get('routes.base');

        return $this;
    }

    /**
     * Get application registry.
     *
     * @return BRegistry
     */
    public function getRegistry() {
        return $this->_registry;
    }

    /**
     * Get last request object.
     *
     * @return BRequest
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * Get the front controller.
     *
     * @return BFrontController|BSingleton
     */
    public function getFrontController() {
        return $this->_frontController;
    }

    /**
     * Set registry.
     *
     * @param BRegistry $registry
     */
    protected function setRegistry(BRegistry $registry) {
        $this->_registry = $registry;
    }

    /**
     * Set request.
     *
     * @param BRequest $request
     */
    protected function setRequest(BRequest $request) {
        $this->_request = $request;
    }

    /**
     * Set front controller.
     *
     * @param BFrontController $frontController
     */
    protected function setFrontController(BFrontController $frontController) {
        $this->_frontController = $frontController;
    }

    /**
     * Get application layout/
     *
     * @return BLayout
     */
    public function getLayout() {
        return $this->_layout;
    }

    /**
     * Set application layout.
     *
     * @param BLayout $layout
     * @return \BApplication
     */
    public function setLayout(BLayout $layout) {
        $this->_layout = $layout;
        return $this;
    }

    /**
     * Enable the application layout using for output.
     *
     * @return BApplication
     */
    public function enableLayout() {
        $this->_layoutEnabled = TRUE;
        return $this;
    }

    /**
     * Disable the application layout using for output.
     * @return BApplication
     */
    public function disableLayout() {
        $this->_layoutEnabled = FALSE;
        return $this;
    }

    /**
     * Get layout status.
     *
     * @return bool
     */
    public function layoutEnabled() {
        return $this->_layoutEnabled;
    }

    /**
     * Register application module.
     *
     * @param        $moduleName
     * @param bool   $isDefault
     * @param string $moduleDir
     */
    public function registerModule($moduleName, $isDefault = FALSE, $moduleDir = '') {
        if (!$this->getRegistry()->exists('modules')) {
            $this->getRegistry()->set($moduleName, array());
        }

        $moduleInfo            = new stdClass();
        $moduleInfo->name      = $moduleName;
        $moduleInfo->dir       = $moduleDir ? $moduleDir : BT_MODULES_PATH . $moduleName;
        $moduleInfo->allowed   = TRUE;
        $moduleInfo->isDefault = $isDefault;

        $modules              = $this->getRegistry()->get('modules');
        $modules[$moduleName] = $moduleInfo;

        $this->getRegistry()->set('modules', $modules);
        BAutoloader::addPath($moduleInfo->dir . DS . 'controllers' . DS, $moduleInfo->dir . DS . 'models' . DS);

    }

    /**
     * Run the application.
     */
    public function run() {
        $response = $this->getFrontController()->dispatch();

        if ($this->layoutEnabled()) {
            $response->setLayout($this->getLayout());
        }

        $response->sendOut();
    }

    /**
     * Get base URL.
     *
     * @return string
     */
    public function getBaseUrl() {
        return $this->_baseUrl;
    }
}
