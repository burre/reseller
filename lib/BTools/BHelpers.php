<?php

/**
 * Project:   BTools
 * File:      BHelpers.php
 * Date:      15.01.2012
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Miscellaneous helpers.
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */

class BHelpers
{

    /**
     * Clear the user input data.
     *
     * @param string $value
     *
     * @return string
     */
    public static function clearUserInput($value) {
        return htmlspecialchars(addslashes(trim(strip_tags($value))), ENT_QUOTES);
    }

    /**
     * Clear the array of user input data.
     *
     * @param array $array
     *
     * @return array
     */
    public static function clearUserInputArray(array $array) {
        foreach ($array as $key => $value) {
            $array[$key] = self::clearUserInput($value);
        }
        return $array;
    }

    /**
     * Parse the standard config file (.ini).
     * Parsing going with a sections processing.
     *
     * @param string $config_file
     *
     * @throws Exception
     * @return array $array
     */
    public static function parseConfig($config_file) {
        if (!empty($config_file)) {
            if (file_exists($config_file)) {
                $retval = array();
                $config = parse_ini_file($config_file, TRUE);
                if ($config !== FALSE) {
                    if (isset($config['common']['mode'])) {
                        foreach ($config['common'] as $key => $val) {
                            $retval[$key] = $val;
                        }
                        if (isset($config[$config['common']['mode']])) {
                            foreach ($config[$config['common']['mode']] as $key => $val) {
                                $retval[$key] = $val;
                            }
                        }
                    } else {
                        foreach ($config as $key => $val) {
                            $retval[$key] = $val;
                        }
                    }
                } else {
                    throw new Exception("Error parsing config file: $config_file");
                }
                return $retval;
            } else {
                throw new Exception("Config file not found: $config_file");
            }
        }
    }

    /**
     * Checks for valid email.
     *
     * @param string $email
     *
     * @return boolen
     */
    public static function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Transform date to MySQL format 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM:SS'.
     *
     * @param int  $timestamp
     * @param bool $short
     *
     * @return string
     */
    public static function mysqlDate($timestamp = 0, $short = TRUE) {
        $timestamp OR $timestamp = time();
        $format = $short ? 'Y-m-d' : 'Y-m-d H-i-s';
        return date($format, $timestamp);
    }
}
