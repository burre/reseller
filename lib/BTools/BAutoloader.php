<?php

/**
 * Project:   BTools
 * File:      BAutoloader.php 
 * Date:      10.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Autoloader class.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
class BAutoloader {

    /**
     * Array of pathes to find new classes.
     * @var array 
     */
    private static $_path = array();

    /**
     * Add one or more pathes at once into array.
     *
     * @inter
     * @internal this uses implicit parameters e.g. $path1 [,$path2, $path3, ...]
     * @return void
     */
    public static function addPath() {
        $args = func_get_args();

        foreach ($args as $dir) {
            self::$_path[] = $dir;
        }
    }

    /**
     * Registers a new spl_autoload function.
     * If any function are registered, our function 
     * will prepend existing stack. Otherwise our 
     * function will appended into stack.
     * 
     * @throws Exception 
     * @return void
     */
    public static function register() {
        if (spl_autoload_functions()) {
            if (!spl_autoload_register(array(__CLASS__, 'autoload'), TRUE, TRUE)) {
                throw new Exception('BAutoloader: Could not register autoload function (prepend).');
            }
        } else {
            if (!spl_autoload_register(array(__CLASS__, 'autoload'))) {
                throw new Exception('BAutoloader: Could not register autoload function (append).');
            }
        }
    }

    /**
     * Unregisters our autoload function.
     * 
     * @throws Exception 
     */
    public static function unregister() {
        if (!spl_autoload_unregister(array(__CLASS__, 'autoload'))) {
            throw new Exception('BAutoloader: Could not unregister autoload function.');
        }
    }

    /**
     * Loads new class automatically by class name.
     * 
     * @param string $className
     * @return boolean The loading success.
     */
    public static function autoload($className) {
        $success = FALSE;

        foreach (self::$_path as $path) {
            $file = $path . $className . '.php';
            
            if (file_exists($file)) {
                $success = TRUE;
                require_once($file);
            }
        }

        return $success;
    }
    
}
