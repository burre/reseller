<?php
/**
 * Project:   BTools
 * File:      BRequest.php
 * Date:      23.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BRequest
 *
 * @uses      BObject
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 *
 * @method BRequest setGet(stdClass $_GET)
 * @method stdClass getGet()
 * @method BRequest setPost(stdClass $_POST)
 * @method stdClass getPost()
 * @method BRequest setCookies(stdClass $_COOKIE)
 * @method stdClass getCookies()
 * @method BRequest setRequest(stdClass $_REQUEST)
 * @method stdClass getRequest()
 * @method BRequest setServer(stdClass $_SERVER)
 * @method stdClass getServer()
 * @method BRequest setFiles(stdClass $_FILES)
 * @method stdClass getFiles()
 * @method BRequest setEnv(stdClass $_ENV)
 * @method stdClass getEnv()
 *
 * @property stdClass $get
 * @property stdClass $post
 * @property stdClass $cookies
 * @property stdClass $request
 * @property stdClass $server
 * @property stdClass $files
 * @property stdClass $env
 */
class BRequest extends BObject
{
    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->setStructure(
            array('get', 'post', 'cookies', 'request', 'server', 'session', 'files', 'env'),
            array('dispatched'),
            self::BO_STRICT_STRUCTURE
        );

        $this->setGet((object)$_GET);
        $this->setPost((object)$_POST);
        $this->setCookies((object)$_COOKIE);
        $this->setRequest((object)$_REQUEST);
        $this->setServer((object)$_SERVER);
        $this->setFiles((object)$_FILES);
        $this->setEnv((object)$_ENV);

        $this->setOption('dispatched', FALSE);
    }

    /**
     * Get all session variables or a specified only.
     *
     * @param null $key
     *
     * @return array|null
     */
    public function getSession($key = NULL) {
        if ($key) {
            return isset($_SESSION[$key]) ? $_SESSION[$key] : NULL;
        } else {
            return $_SESSION;
        }
    }

    /**
     * Set sesion variable.
     *
     * @param $key
     * @param $value
     */
    public function setSession($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     * Delete session variable.
     *
     * @param $key
     */
    public function delSession($key) {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * Remote IP address.
     *
     * @return string
     */
    public function ip() {
        return $this->server->REMOTE_ADDR;
    }

    /**
     * Request URI.
     *
     * @return string
     */
    public function uri() {
        return $this->server->REQUEST_URI;
    }

    /**
     * Query string..
     *
     * @return string
     */
    public function queryString() {
        return $this->server->QUERY_STRING;
    }

    /**
     * Host name.
     *
     * @return string
     */
    public function host() {
        return $this->server->HTTP_HOST;
    }

    /**
     * HTTP protocol.
     *
     * @return string
     */
    public function protocol() {
        // $this->server->SERVER_PROTOCOL
        return 'http://';
    }

    /**
     * Whole URL.
     *
     * @return string
     */
    public function url() {
        return $this->protocol() . $this->host() . $this->uri();
    }

    /**
     * HTTP referer.
     *
     * @return string
     */
    public function referer() {
        return isset($this->server->HTTP_REFERER) ? $this->server->HTTP_REFERER : NULL;
    }

    /**
     * Is last request a POST?
     *
     * @return bool
     */
    public function isPost() {
        return $this->server->REQUEST_METHOD == 'POST';
    }

    /**
     * Is last request a AJAX?
     *
     * @return bool
     */
    public function isAjax() {
        $xhr = $this->server->HTTP_X_REQUESTED_WITH;
        return isset($xhr) && $xhr === 'XMLHttpRequest';
    }

    /**
     * Checks the request processing status.
     *
     * @return bool
     */
    public function isDispatched() {
        return $this->getOption('dispatched');
    }

    /**
     * Set the route processing status.
     *
     * @param bool $status
     *
     * @return \BRequest
     */
    public function setDispatched($status = TRUE) {
        $this->setOption('dispatched', $status);
        return $this;
    }

}
