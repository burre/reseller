<?php

/**
 * Project:   BTools
 * File:      BFormValidatorInterface.php 
 * Date:      25.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BFormValidator
 *
 * @uses      parent_class_name
 * @package   package_name 
 * @author    Victor Burak <vb@atompark.com>
 */
interface BFormValidatorInterface {
    
    public function validate();
    
}
