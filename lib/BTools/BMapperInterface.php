<?php

/**
 * Project:   BTools
 * File:      BMapperInterface.phpp
 * Date:      20.11.2011
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Data model interface.
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
interface BMapperInterface {

    public function create();

    public function delete();

    public function load();

    public function save();
}