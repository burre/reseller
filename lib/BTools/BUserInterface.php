<?php
/**
 * Project:   BTools
 * File:      BUserInterface.php
 * Date:      27.08.12
 *
 * @package   Btools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BUserInterface
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
interface BUserInterface
{
    public function getRole();

    public function setRole($role);

    public function isLoggedIn();

    public function setLoggedIn($status = TRUE);

    public function getName();

    public function getLogin();

}
