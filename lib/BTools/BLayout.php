<?php
/**
 * Project:   BTools
 * File:      BLayout.php
 * Date:      24.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BLayout
 *
 * @uses      parent_class_name
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 */
class BLayout extends BView
{

    public function __construct() {
        parent::__construct();
        // to extend options structure
        //$options = $this->getStructure(self::BO_OPTIONS);
        //$options[] = 'optName';
        //$this->setStructure(
        //    array(), // in the data structure will store only view's variables
        //    $options,
        //    self::BO_FLUID_STRUCTURE
        //);
    }


}
