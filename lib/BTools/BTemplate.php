<?php
/**
 * WARNING! This file is deprecated, bse BView instead of it.
 *
 * Project:   BTools
 * File:      BTemplate.php
 * Date:      20.11.2011
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Template class.
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BTemplate {

    /**
     * Directory name where templates are stored.
     *
     * @var string
     */
    private $_dir;

    /**
     * Current template file name.
     *
     * @var string
     */
    private $_file;

    /**
     * Template vars.
     *
     * @var array
     */
    private $_vars = array();

    /**
     * Debug mode status;
     *
     * @var boolean
     */
    private $_debug = FALSE;

    /**
     * Create new Template object.
     *
     * @param string $file
     *
     * @return \BTemplate
     */
    function __construct($file = '') {
        if (!empty($file)) {
            $this->setFile($file);
        }
    }

    /**
     * Parses a template file and returns complete code.
     *
     * @internal param string $file
     * @return string
     */
    public function parse() {
        ob_start();
        extract($this->_vars);  // backward compatibility
        require($this->_dir . $this->_file);
        return $this->_replaceTags(ob_get_clean());
    }

    /**
     * Render a template file.
     *
     * @internal param string $file
     * @return void
     */
    public function render() {
        echo $this->parse();
    }

    /**
     * Set a template variable.
     *
     * @param string $name
     * @param mixed $val
     * @return \BTemplate
     */
    public function set($name, $val) {
        if (!empty($name)) {
            $this->_vars[$name] = $val;
        }
        return $this;
    }

    /**
     * Set template file.
     *
     * @param string $file
     * @return BTemplate | boolean
     * @throws Exception
     */
    public function setFile($file) {
        if (!empty($file) AND file_exists($this->_dir . $file)) {
            $this->_file = $file;
            return $this;
        } else {
            throw new Exception('BTemplate: File does not exists!');
        }
    }

    /**
     * Sets directory path where templates are stored.
     *
     * @param string $dir
     * @return BTemplate | boolean
     * @throws Exception
     */
    public function setDir($dir) {
        if (!empty($dir) AND file_exists($dir)) {
            $this->_dir = $dir;
            return $this;
        } else {
            throw new Exception('BTemplate: Directory does not exists!');
        }
    }

    /**
     * Clear a certain template variable or whole array.
     *
     * @param string $var | none
     * @return void
     */
    public function clear($var = NULL) {
        if (isset($var) && array_key_exists($var, $this->_vars)) {
            unset($this->_vars[$var]);
        } else {
            $this->_vars = array();
        }
    }

    /**
     * Turn on/off debug mode.<br />
     * In debug mode if tag has been not found into variable list,
     * then it name will be displayed in curly brackets.
     * Otherwise, if debug mode is off (FALSE) empty string will be
     * displayed (recommended for production).
     *
     * @param boolean $mode
     * @return BTemplate
     */
    public function setDebugMode($mode = TRUE) {
        $this->_debug = $mode;
        return $this;
    }

    /**
     * Replaces all tags enclosed into curly brackets
     * with a data from the template variable list.
     *
     * @param string $tpl
     * @return string
     */
    private function _replaceTags($tpl) {
        $this->_vars = array_change_key_case($this->_vars, CASE_UPPER);
        $tpl = preg_replace_callback('/\{\w+\}/', array($this, '_replaceTagsCallback'), $tpl);

        return $tpl;
    }

    /**
     * Callback function for tags replacing.
     *
     * @param array $matches
     * @return string
     */
    private function _replaceTagsCallback($matches) {
        $varName = str_replace(array('{', '}'), '', $matches[0]);
        $varName = strtoupper($varName);

        if (array_key_exists($varName, $this->_vars)) {
            $retval = $this->_vars[$varName];
        } else {
            $retval = $this->_debug ? $matches[0] : '';
        }

        return $retval;
    }
}
