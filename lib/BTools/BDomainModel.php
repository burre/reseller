<?php
/**
 * Project:   BTools
 * File:      BDomainModel.php
 * Date:      21.08.12
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BDomainModel
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BDomainModel
{
    /**
     * Registered mappers.
     *
     * @var array
     */
    protected $_mappers = array();

    /**
     * Model data.
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Default collection type
     *
     * @var string
     */
    protected $_collectionType = '';

    /**
     * Add mapper to DB-table.
     *
     * @param string                  $name
     * @param BDBMapperAbstract       $mapper
     */
    protected function _addMapper($name, BDBMapperAbstract $mapper) {
        $this->_mappers[$name] = $mapper;
    }

    /**
     * Get specified mapper.
     *
     * @param string $name
     *
     * @return BDBMapperAbstract | bool
     */
    protected function _getMapper($name = '') {
        if (isset($this->_mappers[$name])) {
            return $this->_mappers[$name];
        } else {
            return FALSE;
        }
    }

    /**
     * Set data from array.
     * Elements with identical keys will be replaced,
     * otherwise new array will be added to existing one.
     *
     * @param array $data
     *
     * @return \BDomainModel
     */
    protected function _setData(array $data) {
        $this->_data = array_merge($this->_data, $data);
        return $this;
    }

    /**
     * Make a collection from multi row result.
     *
     * @param array $result
     *
     * @param       $type
     *
     * @return BCollectionBase
     */
    protected function _makeCollection(array $result, $type = FALSE) {
        if (!$type) {
            $type = $this->_collectionType;
        }

        $collection = new $type();

        if (!empty($result)) {
            foreach ($result as $row) {
                $user = new $this;
                $user->_setData($row);
                $collection->add($user);
            }
        }

        return $collection;
    }

    /**
     * Find one result by specified field.
     * If search successful - mapper data wil be copied into model.
     *
     * @param $fieldName
     * @param $value
     *
     * @return bool
     */
    public function findOneBy($fieldName, $value) {
        $retval = FALSE;

        foreach ($this->_mappers as $mapper) {
            /** @var $mapper BDBMapperAbstract */
            if ($mapper->getDbTable()->hasField($fieldName)) {
                if ($retval = $mapper->findOneBy($fieldName, $value)) {
                    $this->_setData($mapper->getMapperData());
                    break;
                }
            }
        }

        return $retval;
    }

    /**
     * Overloading for quick field access.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name) {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[$name];
        } else {
            return NULL;
        }
    }

    /**
     * Overloading for quick field updating.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return void
     */
    public function __set($name, $value) {
        if (array_key_exists($name, $this->_data)) {
            $this->_data[$name] = $value;
        }
    }
}
