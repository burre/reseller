<?php

/**
 * Project:   BTools
 * File:      BRegistry.php
 * Date:      10.01.2012
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Registry class.
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */

class BRegistry
{

    private $_info = 'BRegistry Class instance';
    private $_registry = array();

    /**
     * BRegistry constructor.
     * return void
     */
    public function __construct() {

    }

    /**
     * Set variable to registry.
     *
     * @param string $name
     * @param mixed  $val
     *
     * @return BRegistry
     */
    public function set($name, $val) {
        $this->_registry[$name] = $val;
        return $this;
    }

    /**
     * Get variable from registry.
     * Return NULL if name does not exists in registry.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get($name) {
        if ($this->exists($name)) {
            return $this->_registry[$name];
        } else {
            return NULL;
        }
    }

    /**
     * Clear variable in registry.
     *
     * @param string $name
     *
     * @return BRegistry
     */
    public function clear($name) {
        if ($this->exists($name)) {
            $this->_registry[$name] = NULL;
        }
        return $this;
    }

    /**
     * Delete variable from registry.
     *
     * @param string $name
     *
     * @return BRegistry
     */
    public function del($name) {
        if ($this->exists($name)) {
            unset($this->_registry[$name]);
        }
        return $this;
    }

    /**
     * Check if variable is exists in registry.
     *
     * @param string $name
     *
     * @return bool
     */
    public function exists($name) {
        return array_key_exists($name, $this->_registry);
    }

    /**
     * Get whole registry as array.
     *
     * @return array
     */
    public function getAllVars() {
        return $this->_registry;
    }

    /**
     * Add array to registry.
     *
     * @param array $array
     *
     * @return BRegistry
     */
    public function addArray($array) {
        if (is_array($array)) {
            foreach ($array as $name => $val) {
                $this->set($name, $val);
            }
        }
        return $this;
    }

}
