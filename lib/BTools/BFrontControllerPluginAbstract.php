<?php
/**
 * Project:   BTools
 * File:      BFrontControllerPluginAbstract.php
 * Date:      31.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BFrontControllerPluginAbstract
 *
 * @uses      parent_class_name
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BFrontControllerPluginAbstract
{
    /**
     * @var BRequest
     */
    protected $_request = NULL;

    /**
     * @var BResponse
     */
    protected $_response = NULL;

    /**
     * @var BApplication
     */
    protected $_app = NULL;

    /**
     * @var BFrontController
     */
    protected $_front = NULL;

    public function routeStartup() {
    }

    public function routeShutdown() {
    }

    public function dispatchLoopStartup() {
    }

    public function dispatchLoopShutdown() {
    }

    public function preDispatch() {
    }

    public function postDispatch() {
    }

    public function __construct() {
        $this->_app   = BApplication::getInstance();
        $this->_front = BFrontController::getInstance();
    }

    public function setRequest(BRequest $request) {
        $this->_request = $request;
        return $this;
    }

    public function getRequest() {
        return $this->_request;
    }

    public function setResponse(BResponse $response) {
        $this->_response = $response;
        return $this;
    }

    public function getResponse() {
        return $this->_response;
    }

}
