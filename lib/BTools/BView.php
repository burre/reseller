<?php
/**
 * Project:   BTools
 * File:      BView.php
 * Date:      20.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BView
 *
 * @uses      BTemplate
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BView extends BObject
{
    protected $_helpers = array();

    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->setStructure(
            array(), // in the data structure will store only view's variables
            array('viewPath', 'viewFile', 'debugMode', 'ci_data'),
            self::BO_FLUID_STRUCTURE
        );
        $this->setDebugMode(FALSE);
    }

    /**
     * Parses a template file and returns complete code.
     *
     * @return string
     */
    public function parse() {
        if (file_exists($this->getViewFile())) {
            ob_start();
            extract($this->_data); // backward compatibility
            require($this->getOption('viewPath') . $this->getOption('viewFile'));
            return $this->_replaceTags(ob_get_clean());
        } else {
            return '';
        }
    }

    /**
     * Render a template file.
     *
     * @internal param string $file
     * @return void
     */
    public function render() {
        echo $this->parse();
    }

    /**
     * Parses the partial block.
     *
     * @param $blockFileName
     *
     * @return string
     */
    public function parseBlock($blockFileName) {
        $file = $this->getOption('viewPath') . 'blocks' . DS . $blockFileName;

        if (file_exists($file)) {
            ob_start();
            extract($this->_data); // backward compatibility
            require($file);
            return $this->_replaceTags(ob_get_clean());
        } else {
            return '';
        }
    }

    /**
     * Parses the partial block.
     *
     * @param $blockFileName
     */
    public function renderBlock($blockFileName) {
        echo $this->parseBlock($blockFileName);
    }

    /**
     * Turn on/off debug mode.<br />
     * In debug mode if tag has been not found into variable list,
     * then it name will be displayed in curly brackets.
     * Otherwise, if debug mode is off (FALSE) empty string will be
     * displayed (recommended for production).
     *
     * @param boolean $mode
     *
     * @return BTemplate
     */
    public function setDebugMode($mode = TRUE) {
        $this->setOption('debugMode', $mode);
        return $this;
    }

    /**
     * Check output mode.
     *
     * @return mixed
     */
    public function isDebugMode() {
        return $this->getOption('debugMode');
    }

    /**
     * Replaces all tags enclosed into curly brackets
     * with a data from the template variable list.
     *
     * @param string $tpl
     *
     * @return string
     */
    private function _replaceTags($tpl) {
        $ci_data = array();
        foreach ($this->_data as $key => $val) {
            $ci_data[strtoupper($key)] = $key;
        }
        $this->setOption('ci_data', $ci_data);
        //$this->setOption('ci_data', array_change_key_case($this->_data, CASE_UPPER));
        $tpl = preg_replace_callback('/\{\w+\}/', array($this, '_replaceTagsCallback'), $tpl);

        return $tpl;
    }

    /**
     * Callback function for tags replacing.
     *
     * @param array $matches
     *
     * @return string
     */
    private function _replaceTagsCallback($matches) {
        $varName = str_replace(array('{', '}'), '', $matches[0]);
        $varName = strtoupper($varName);
        $ci_data = $this->getOption('ci_data');

        if (array_key_exists($varName, $this->_data)) {
            $retval = $this->_data[$varName];
        } elseif (array_key_exists($varName, $ci_data)) {
            $retval = $this->_data[$ci_data[$varName]];
        } else {
            $retval = $this->isDebugMode() ? $matches[0] : '';
        }

        return $retval;
    }

    /**
     * Get the view file path.
     *
     * @return string
     */
    public function getViewFile() {
        return $this->getOption('viewPath') . $this->getOption('viewFile');
    }

    /**
     * Set the view file name.
     *
     * @param $file
     *
     * @return BView
     */
    public function setViewFile($file) {
        $this->setOption('viewFile', $file);
        return $this;
    }

    /**
     * Get the view directory path.
     *
     * @return string
     */
    public function getViewPath() {
        return $this->getOption('viewPath');
    }

    /**
     * Set the view directory path.
     *
     * @param $path
     *
     * @return BView
     */
    public function setViewPath($path) {
        $this->setOption('viewPath', $path);
        return $this;
    }

}
