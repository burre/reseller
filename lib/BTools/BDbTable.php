<?php

/**
 * Project:   BTools
 * File:      BDbTable.php
 * Date:      20.11.2011
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database table class.
 *
 * @uses      BDbTableAbstract, BDbTableInterface
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BDbTable extends BDbTableAbstract implements BDbTableInterface
{

    /**
     * The table name.
     *
     * @var string
     */
    private $_name;

    /**
     * Name of a primary key field.
     *
     * @todo Possibly predefined 'id' is a bad solution, but it's simple and quick.
     * @var string
     */
    private $_primary = 'id';

    /**
     * Common table structure via DESCRIBE query.
     *
     * @var array
     */
    private $_structure = array();

    /**
     * List of fields name.
     *
     * @var array
     */
    private $_fields = array();

    /**
     * One data row with all fields.
     *
     * @var stdClass
     */
    private $_row;

    /**
     * Create DbTable object.
     */
    public function __construct() {
        $this->grabStructure()->grabFieldList();
    }

    /**
     * Check if the table has a field.
     *
     * @param string $field_name
     *
     * @return bool
     */
    public function hasField($field_name) {
        return in_array($field_name, $this->_fields);
    }

    /**
     * Get the fields list.
     *
     * @return array
     */
    public function getFieldList() {
        return $this->_fields;
    }

    /**
     * Get the table row.
     *
     * @return string
     */
    public function getRow() {
        return $this->_row;
    }

    /**
     * Get the table name.
     *
     * @return string
     */
    public function getTableName() {
        return $this->_name;
    }

    /**
     * Set the table name.
     *
     * @param string @name
     *
     * @return string
     */
    public function setTableName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * Get the name of a primary key field.
     *
     * @return string
     */
    public function getPrimaryKey() {
        return $this->_primary;
    }

    /**
     * Set the name of a primary key field.
     *
     * @return string
     */
    public function setPrimaryKey($name) {
        $this->_primary = $name;
        return $this;
    }

    /**
     * Get common table structure.
     *
     * @return DbTable
     */
    protected function grabStructure() {
        if (!$this->_structure) {
            if ($this->getAdapter()->query("DESCRIBE `" . $this->getTableName() . "`"))
                $this->_structure = $this->getAdapter()->getRowsetAsArray();
            $temp = array();

            foreach ($this->_structure as $field) {
                $temp[$field['Field']] = (object)$field;
            }

            $this->_structure = (object)$temp;
        } else {
            $this->_structure = array();
        }

        return $this;
    }

    /**
     * Get object with table structure info.
     *
     * @return stdClass
     */
    public function getStructure() {
        if (!$this->_structure) {
            $this->grabStructure();
        }
        return $this->_structure;
    }


    /**
     * Get the table's field list.
     *
     * @return DbTable
     */
    protected function grabFieldList() {
        if (!$this->_fields) {
            if (!$this->_structure) {
                $this->grabStructure();
            }

            foreach ((array)$this->_structure as $row) {
                $this->_fields[] = $row->Field;
            }
        }

        return $this;
    }

    /**
     * Find row by primary key (default 'id').
     *
     * @param integer $val
     *
     * @return mysqli_result
     */
    public function findByPrimaryKey($val) {
        return $this->select("`{$this->getPrimaryKey()}` = $val");
    }

    /**
     * Insert new record (1 of CRUD).
     *
     * @param array $data
     *
     * @return DbTable | FALSE on error
     */
    public function insert(array $data) {
        $table  = $this->getTableName();
        $fields = $this->getFieldList();
        $data   = array_slice($data, 0, count($this->getFieldList()), TRUE);
        $values = '';

        if ($fields[0] == $this->getPrimaryKey()) {
            unset($fields[0]);
            unset($data[$this->getPrimaryKey()]);
        }

        foreach ($fields as $key => $val)
            $fields[$key] = "`$val`";

        foreach ($data as $key => $val) {
            // if the field type is one of string types - enclose the value to single quotes.
            if (preg_match('/char|text|date|time|blob/i', $this->_structure->$key->Type)) {
                $data[$key] = "'{$this->getAdapter()->escape($val)}'";
            }
        }

        $fields = implode(', ', $fields);
        $values = implode(', ', $data);
        $sql    = 'INSERT INTO `%s` (%s) VALUES(%s)';
        $sql    = sprintf($sql, $table, $fields, $values);

        if ($this->getAdapter()->query($sql)->getLastResult()) {
            return $this->getAdapter()->getInsertId();
        } else {
            return FALSE;
        }
    }

    /**
     * Get one or more records (2 of CRUD).
     *
     * @param string $where WHERE condition (must be escaped before).
     * @param string $order ORDER condition
     * @param string $limit LIMIT value
     *
     * @return array rowset
     */
    public function select($where, $order = NULL, $limit = NULL) {
        $table = $this->getTableName();
        $where = "WHERE $where";
        $order = empty($order) ? '' : $order = "ORDER BY $order";
        $limit = empty($limit) ? '' : $limit = "LIMIT $limit";
        $sql   = "SELECT * FROM `%s` %s %s %s";
        $sql   = sprintf($sql, $table, $where, $order, $limit);
        $sql   = trim($sql);

        $this->getAdapter()->query($sql);
        return $this->getAdapter()->getRowsetAsArray();
    }

    /**
     * Update record (3 of CRUD).
     *
     * @TODO refactor this according to insert() method.
     *
     * @param array $data
     *
     * @return mysqli_result
     */
    public function update(array $data) {
        $table  = $this->getTableName();
        $fields = $this->getFieldList();
        $id     = is_numeric($data[$this->getPrimaryKey()]) ? (int)$data[$this->getPrimaryKey()] : $data[$this->getPrimaryKey()];
        $where  = "WHERE `{$this->getPrimaryKey()}` = $id";
        $data   = array_slice($data, 0, count($this->getFieldList()), TRUE);
        $values = '';

        if ($fields[0] == $this->getPrimaryKey()) {
            unset($fields[0]);
            unset($data[$this->getPrimaryKey()]);
        }

        foreach ($data as $key => $val) {
            if (is_numeric($val)) {
                $val = (int)$val;
            } elseif (is_float($val)) {
                $val = (float)$val;
            } elseif (is_string($val)) {
                $val = "'{$this->getAdapter()->escape($val)}'";
            } elseif (empty($val)) {
                $val = "''";
            }
            $values .= "`$key` = $val, ";
        }

        $values = substr($values, 0, -2); // delete last comma
        $sql    = 'UPDATE `%s` SET %s %s';
        $sql    = sprintf($sql, $table, $values, $where);

        return $this->getAdapter()->query($sql)->getLastResult();
    }

    /**
     * Delete record (4 of CRUD).
     *
     * @param $id
     *
     * @return mysqli_result
     */
    public function delete($id) {
        $table = $this->getTableName();
        $where = "WHERE `{$this->getPrimaryKey()}` = $id";
        $sql   = 'DELETE FROM `%s` %s';
        $sql   = sprintf($sql, $table, $where);

        return $this->getAdapter()->query($sql)->getLastResult();
    }

    public function needQuotes($fieldName) {
        $numeric = array('tinyint', 'smallint', 'mediumint', 'int', 'bigint',
            'decimal', 'float', 'double', 'real', 'bit', 'boolean', 'serial');
        $type    = $this->getStructure()->$fieldName->Type;
        $type    = preg_replace('/\(.*\)/', '', $type);
        $retval  = TRUE;

        if (in_array($type, $numeric)) {
            $retval = FALSE;
        }

        return $retval;
    }

}

