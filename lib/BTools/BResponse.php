<?php
/**
 * Project:   BTools
 * File:      BResponse.php
 * Date:      24.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BResponse
 *
 * @uses      BObject
 * @package   package_name
 * @author    Victor Burak <vb@atompark.com>
 *
 * @method array getBody()
 * @method BResponse setResponseCode(int $code)
 * @method int  getResponseCode()
 * @method BResponse setFlashMessage(sting $text)
 *
 * @property array  body
 * @property array  headers
 * @property array  cookies
 * @property string flashMessage
 * @property int    responseCode
 *
 */
class BResponse extends BObject
{
    /**
     * @var BLayout
     */
    protected $_layout;

    /**
     * @var array
     */
    protected $_respCodes = array(
        '200' => 'HTTP/1.1 200 OK',
        '301' => 'HTTP/1.1 301 Moved Permanently',
        '304' => 'HTTP/1.1 304 Not Modified',
        '403' => 'HTTP/1.0 403 Forbidden',
        '404' => 'HTTP/1.0 404 Not Found',
        '500' => 'HTTP/1.1 500 Internal Server Error',
    );

    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->setStructure(
            array('body', 'headers', 'cookies', 'flashMessage', 'responseCode'),
            array('isRedirect', 'renderException'),
            self::BO_STRICT_STRUCTURE
        );

        $this->setBody(array());
        $this->setHeaders(array());
        $this->setCookies(array());
        $this->setFlashMessage('');
        $this->setResponseCode(200);
        $this->setOption('isRedirect', FALSE);
        $this->setOption('renderException', FALSE);

    }

    /**
     * Append content block.
     *
     * @param $partName
     * @param $partContent
     *
     * @return BResponse
     */
    public function appendBody($partName, $partContent) {
        $this->_data['body'][$partName] = $partContent;
        return $this;
    }

    /**
     * Append custom headers.
     *
     * @param $name
     * @param $content
     *
     * @return BResponse
     */
    public function appendHeaders($name, $content) {
        $this->_data['headers'][$name] = $content;
        return $this;
    }

    /**
     * Append cookie for setting down.
     *
     * @param BCookie $cookie
     * @param int     $action
     *
     * @return BResponse
     */
    public function appendCookies(BCookie $cookie, $action = BCookie::BT_COOKIE_SET) {
        $this->_data['cookies'][$cookie->name] = array('cookie' => $cookie, 'action' => $action);
        return $this;
    }

    /**
     * Write all cookies.
     *
     * @return BResponse
     */
    public function writeCookies() {
        if (!empty($this->cookies)) {
            foreach ($this->cookies as $name => $item) {
                // @TODO process the result for notification about error
                if ($item['action'] == BCookie::BT_COOKIE_SET) {
                    $item['cookie']->set();
                } elseif ($item['action'] == BCookie::BT_COOKIE_REMOVE) {
                    $item['cookie']->remove();
                }
            }
        }

        return $this;
    }

    /**
     * Add application layout.
     *
     * @param BLayout $layout
     *
     * @return BResponse
     */
    public function setLayout(BLayout $layout) {
        $this->_layout = $layout;
        return $this;
    }

    /**
     * Get application layout.
     *
     * @return BLayout
     */
    public function getLayout() {
        return $this->_layout;
    }

    /**
     * Send full response to browser.
     */
    public function sendOut() {
        if ($this->responseCode != 200) {
            header($this->_respCodes[$this->responseCode]);
        }

        $this->writeCookies();

        $this->sendHeaders();

        $this->outputBody();
    }

    /**
     * Print out response.
     */
    public function outputBody() {
        $layout = $this->getLayout();

        if ($layout instanceof BLayout) {
            // normal way
            $layout->setData($this->body);
            $layout->render();
        } else {
            // without layout we'll show only 'content' section
            echo $this->body['content'];
        }
    }


}
