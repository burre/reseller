<?php

/**
 * Project:   NAD
 * File:      BDatabaseNS.php 
 * Date:      17.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database adapter class (non singletone).
 *
 * @uses      mysqli
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
final class BDatabaseNS extends BDatabaseBase {

    protected $_info = 'BDatabaseNS (Non Singletone)';
    public $charset = '';

    /**
     * Creates Database object and establishes connection.
     * Also sets charset for connection if specified.
     *
     * @param array  $credentials
     * @param string $charset
     * @throws Exception
     * @return BDatabaseNS
     */
    public function __construct($credentials, $charset = '') {
        if ($credentials && is_array($credentials)) {
            !($this->_credentials = new stdClass()) OR
                    $this->_credentials = (object) $credentials;
        } else {
            throw new Exception('Error! No credentials provided for a database access.');
        }

        if (!$this->_connection) {
            $this->_connection = parent::__construct(
                    $this->_credentials->dbHost, 
                    $this->_credentials->dbUser, 
                    $this->_credentials->dbPass, 
                    $this->_credentials->dbName, 
                    $this->_credentials->dbPort
            );

            if ($this->connect_errno) {
                throw new Exception("Connection Error $this->connect_errno: $this->connect_error");
            } else {
                $this->_query = new stdClass();
                $this->_query->sql = '';
                $this->_query->success = NULL;
                $this->_query->start_time = 0;
                $this->_query->end_time = 0;
                $this->_query->spent_time = 0;
                
                if ($charset) {
                    $this->setCharSet($charset);
                }
            }
        }
    }
    
}
