<?php
/**
 * Project:   BTools
 * File:      BPaginator.php
 * Date:      31.08.12
 *
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BPaginator
 *
 * @uses      parent_class_name
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 *
 * @method BPaginator setItemsTotal(int $total)
 * @method int getItemsTotal()
 * @method BPaginator setItemsOnPage(int $items)
 * @method int getItemsOnPage()
 * @method BPaginator setPagesTotal(int $pages)
 * @method int getPagesTotal()
 * @method BPaginator setCurrentPage(int $page)
 * @method int getCurrentPage()
 * @method BPaginator setPagesToShow(int $pages)
 * @method int getPagesToShow()
 * @method BPaginator setAutoHide(bool $hide)   If TRUE - no pagination when all elements are shown on the page.
 * @method bool getAutoHide()
 * @method BPaginator setBaseUrl(string $url)
 * @method string getBaseUrl()
 * @method BPaginator setView(BView $view)
 * @method BView getView()
 * @method BPaginator setTplBlockName(string $tplBlockName)
 * @method string getTplBlockName()
 * @method BPaginator setRangeStart(int $rangeStart)
 * @method int getRangeStart()
 * @method BPaginator setRangeEnd(int $rangeEnd)
 * @method int getRangeEnd()
 */
class BPaginator extends BObject
{
    public function __construct() {
        parent::__construct();
        $this->setStructure(
            array(
                'filter',
                'itemsTotal',
                'itemsOnPage',
                'pagesTotal',
                'currentPage',
                'pagesToShow',
                'autoHide',
                'baseUrl',
                'view',
                'tplBlockName',
                'rangeStart',
                'rangeEnd',
            ),
            array(),
            self::BO_STRICT_STRUCTURE
        );

        $this->setAutoHide(FALSE);
        $this->setItemsOnPage(10);
        $this->setPagesToShow(10); // 0 - for all
    }

    public function getLimitOffset() {
        return $this->getItemsOnPage() * ($this->getCurrentPage() - 1);
    }

    /**
     * Parse the paginator template.
     *
     * @return string
     */
    public function parse() {
        $this->setPagesTotal(ceil($this->getItemsTotal() / $this->getItemsOnPage()));
        $this->_calculateRange();

        if ($this->getPagesTotal() == 1 && $this->getAutoHide()) {
            $this->setPagesToShow(0);
        }

        $this->getView()->paginator = $this;

        return $this->getView()->parseBlock($this->getTplBlockName());
    }

    public function getFirstPageLink() {
        return $this->getBaseUrl();
    }

    public function getLastPageLink() {
        return $this->getBaseUrl() . 'page/' . $this->getPagesTotal() . '/';
    }

    public function getPageLink($num) {
        return $this->getBaseUrl() . 'page/' . $num . '/';
    }

    public function getPagesTotal() {
        return ceil($this->getItemsTotal() / $this->getItemsOnPage());
    }

    public function getNext10Link() {
        $page = $this->getCurrentPage() + 10;
        return $page > $this->getPagesTotal() ? '' : $this->getPageLink($page);
    }

    public function getPrev10Link() {
        $page = $this->getCurrentPage() - 10;
        return $page < 0 ? '' : $this->getPageLink($page);
    }

    /**
     * Calculate first and last pages of range to display.
     *
     * @return void
     */
    protected function _calculateRange() {
        $currentPage = $this->getCurrentPage();
        $pagesToShow = $this->getPagesToShow();
        $pagesTotal  = $this->getPagesTotal();
        $firstHalf   = floor($pagesToShow / 2);
        $lastHalf    = ceil($pagesToShow / 2);
        $start       = ($currentPage - $firstHalf + 1) <= 0 ? 1 : $currentPage - $firstHalf + 1;
        $end         = ($currentPage + $lastHalf) >= $pagesTotal ? $pagesTotal : $start + $pagesToShow - 1;
        $diff        = $end - $start + 1;
        $start       = $diff < $pagesToShow && ($end - $pagesToShow + 1) > 0 ? $end - $pagesToShow + 1 : $start;

        $this->setRangeStart($start);
        $this->setRangeEnd($end);
    }
}
