<?php
/**
 * Project:   BTools
 * File:      BRoute.php
 * Date:      25.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BRoute
 *
 * @uses      BObject
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 *
 * @method BRoute   setModule(string $module)           Set module name.
 * @method BRoute   setController(string $controller)   Set controller name.
 * @method BRoute   setAction(string $action)           Set action name.
 * @method BRoute   setParams(array $params)            Set params array.
 * @method string   getModule()                         Get module name.
 * @method string   getController()                     Get controller name.
 * @method string   getAction()                         Get action name.
 * @method stdClass getParams()                         Get params object.
 *
 * @property string  $module        Module name.
 * @property string  $controller    Controller name.
 * @property string  $action        Action name.
 * @property BObject params         Params array.
 *
 */
class BRoute extends BObject
{
    /**
     * Class controller.
     */
    public function __construct() {
        parent::__construct();

        $this->setStructure(
            array('module', 'controller', 'action', 'params'),
            array('dispatched'),
            self::BO_STRICT_STRUCTURE
        );

        $this->params = new BObject('Route_Params_DTO');
        $this->setDispatched(FALSE);
    }

    /**
     * Checks the route status.
     *
     * @return bool
     */
    public function isDispatched() {
        return $this->getOption('dispatched');
    }

    /**
     * Set the route status.
     *
     * @param bool $status
     *
     * @return BRoute
     */
    public function setDispatched($status = TRUE) {
        $this->setOption('dispatched', $status);
        return $this;
    }

    /**
     * Assemble URL string for current or specified route.
     *
     * @param array $data
     *
     * @return string
     */
    public function assembleUrl(array $data = array()) {
        if (!empty($data)) {
            $retval = '/' . implode('/', $data) . '/';
        } else {
            $retval = '/' . $this->module . '/' . $this->controller . '/' . $this->action . '/';
        }

        return $retval;
    }
}
