<?php
/**
 * Project:   BTools
 * File:      BObject.php
 * Date:      20.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * BObject is a standard data transfer object (DTO). May be used directly or extended to new class.
 *
 * Access to data is possible through direct reference to the property name or through getters/setters.
 * <pre>    $obj->prop1 = 'xxx' same as $obj->setProp1('xxx')
 *    $x = $obj->prop1 same as $x = $obj->getProp1()</pre>
 *
 * 'Options' is a some data for situative using.
 * <pre>    $obj->setOption('optName', $optValue)
 *    if ($obj->hasOption('optName') {
 *        $x = $obj->getOption('optName')
 *        $obj->delOption('optName')
 *    }</pre>
 *
 * There is possible prevent adding any arbitrary variables - $this->_strictStructure = TRUE.
 *
 * Common case of use:
 *
 * $o = new BObject('objName')
 * $o->setStructure(array('prop1', 'prop2', ...), array('opt1', 'opt2', ...), self::BO_STRICT_STRUCTURE)
 *
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BObject
{
    const BO_STRICT_STRUCTURE = TRUE;
    const BO_FLUID_STRUCTURE  = FALSE;

    const BO_ALL_VARS   = 0;
    const BO_PROPERTIES = 1;
    const BO_OPTIONS    = 2;

    public $objName;

    protected $_data = array();
    protected $_options = array();

    private $_hasChanges = FALSE;
    private $_structure;
    private $_strictStructure = FALSE;

    /**
     * Class constructor.
     *
     * @param string $objName
     */
    public function __construct($objName = '') {
        $this->objName                = $objName;
        $this->_structure             = new stdClass();
        $this->_structure->properties = array();
        $this->_structure->options    = array();
    }

    /**
     * Magic method for direct access, e.g. $value = $obj->somevar.
     *
     * @param string $name
     *
     * @return mixed
     * @throws Exception
     */
    public function __get($name) {
        if (array_key_exists($name, $this->_data)) {
            return $this->_data[$name];
        } else {
            throw new Exception("BObject: Attempt to request an undefined property '{$name}'!");
        }
    }

    /**
     * Magic method for direct access, e.g. $obj->somevar = $value.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @throws Exception
     */
    public function __set($name, $value) {
        if ($this->_isAllowedField($name)) {
            $this->_data[$name] = $value;
            $this->_hasChanges  = TRUE;
        } else {
            throw new Exception("BObject: Attempt to assign not specified field '{$name}' of strict-structured object!");
        }
    }

    /**
     * Magic method for isset() function.
     *
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name) {
        return isset($this->_data[$name]);
    }

    /**
     * Magic method for unset() function.
     *
     * @param string $name
     *
     * @throws Exception
     */
    public function __unset($name) {
        if ($this->_isAllowedField($name)) {
            unset($this->_data[$name]);
            $this->_hasChanges = TRUE;
        } else {
            throw new Exception("BObject: Attempt to unset field '{$name}' of strict-structured object!");
        }
    }

    /**
     * Magic method for calling setters and getters e.g. setVarName($value) or getVarName().
     *
     * @see _set(), __set()
     *
     * @param string $name
     * @param mixed  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments) {
        $retval = $this;

        if (FALSE !== ($pos = strpos($name, 'get')) && $pos == 0) {
            $retval = $this->_get($name);
        }

        if (FALSE !== ($pos = strpos($name, 'set')) && $pos == 0) {
            $this->_set($name, $arguments);
        }

        return $retval;
    }

    /**
     * Get property value by getter name.
     *
     * @param string $name
     *
     * @return mixed
     */
    private function _get($name) {
        $name = str_replace('get', '', $name);
        $name = lcfirst($name);
        return $this->$name;
    }

    /**
     * Set property value by setter name.
     *
     * @param string $name
     * @param mixed  $arguments
     *
     * @throws Exception
     */
    private function _set($name, $arguments) {
        $name = str_replace('set', '', $name);
        $name = lcfirst($name);

        if (is_array($arguments) && !empty($arguments)) {
            $this->$name = $arguments[0];
        } else {
            throw new Exception("BObject: No value is passed to the setter!");
        }
    }

    /**
     * Checks whether field is allowed if the object is strict-structured .
     *
     * @param $name
     *
     * @return bool
     */
    private function _isAllowedField($name) {
        $retval = TRUE;

        if ($this->_strictStructure == self::BO_STRICT_STRUCTURE
            && !in_array($name, $this->getStructure(self::BO_PROPERTIES))
        ) {
            $retval = FALSE;
        }

        return $retval;
    }

    /**
     * Create data structure.
     *
     * @param array $properties
     * @param array $options
     * @param bool  $strict
     *
     * @return \BObject
     */
    public function setStructure(array $properties, array $options = array(), $strict = self::BO_FLUID_STRUCTURE) {
        $this->_structure->properties = $properties;
        $this->_structure->options    = $options;
        $this->_strictStructure       = $strict;
        $this->renew();

        return $this;
    }

    /**
     * Returns the object data structure as array
     * ('properties' or 'options' structure, or both by default).
     *
     * @param int|string $what 'properties', 'options' or none
     *
     * @return array
     */
    public function getStructure($what = self::BO_ALL_VARS) {
        switch ($what) {
            case self::BO_PROPERTIES :
                $retval = $this->_structure->properties;
                break;
            case self::BO_OPTIONS :
                $retval = $this->_structure->options;
                break;
            default :
                $retval = $this->_structure;
                break;
        }

        return (array)$retval;
    }

    /**
     * Set data from array.
     * Elements with identical keys will be replaced,
     * otherwise new array will be added to existing one.
     *
     * @param array $data
     *
     * @return BObject
     */
    public function setData(array $data) {
        $this->_data       = array_merge($this->_data, $data);
        $this->_hasChanges = TRUE;
        return $this;
    }

    /**
     * Rebuild the data structure of object.
     * This will clear all data.
     *
     * @return BObject
     */
    public function renew() {
        foreach ($this->getStructure(self::BO_PROPERTIES) as $property) {
            $this->$property = NULL;
        }

        foreach ($this->getStructure(self::BO_OPTIONS) as $option) {
            $this->_options[$option] = NULL;
        }

        return $this;
    }

    /**
     * Clear all data within existing structure.
     *
     * @return BObject
     */
    public function cleaup() {
        foreach ($this->_data as $key => $val) {
            $this->_data[$key] = NULL;
        }

        foreach ($this->_options as $key => $val) {
            $this->_options[$key] = NULL;
        }

        return $this;
    }

    /**
     * Checks for empty data storage.
     * By default checks only properties.
     *
     * @param int $what
     *
     * @return bool
     */
    public function isEmpty($what = self::BO_PROPERTIES) {
        $retval = FALSE;

        if ($what == self::BO_PROPERTIES || $what == self::BO_ALL_VARS) {
            if (empty($this->_data)) {
                $retval = TRUE;
            } else {
                foreach ($this->_data as $val) {
                    if (empty($val)) {
                        $retval = TRUE;
                        break;
                    }
                }
            }
        }

        if ($what == self::BO_OPTIONS || self::BO_ALL_VARS) {
            if (empty($this->_options)) {
                $retval = TRUE;
            } else {
                foreach ($this->_options as $val) {
                    if (empty($val)) {
                        $retval = TRUE;
                        break;
                    }
                }
            }
        }

        return $retval;
    }

    /**
     * Checks whether the object has any options.
     *
     * @return bool
     */
    public function hasOptions() {
        return !$this->isEmpty(self::BO_OPTIONS);
    }

    /**
     * Checks whether the object has a specified option.
     *
     * @param $name
     *
     * @return bool
     */
    public function hasOption($name) {
        return isset($this->_options[$name]);
    }

    /**
     * Setter for option.
     *
     * @param $name
     * @param $value
     *
     * @return BObject
     */
    public function setOption($name, $value) {
        $this->_options[$name] = $value;
        return $this;
    }

    /**
     * Getter for option.
     *
     * @param $name
     *
     * @throws Exception
     * @return
     */
    public function getOption($name) {
        if (array_key_exists($name, $this->_options)) {
            return $this->_options[$name];
        } else {
            throw new Exception("BObject: Attempt to request an undefined option '{$name}'!");
        }
    }

    /**
     * Clear 'hasChanges' flag.
     *
     * @return BObject
     */
    public function trackChanges() {
        $this->_hasChanges = FALSE;
        return $this;
    }

    /**
     * Check any changes from last trackChanges().
     *
     * @return bool
     */
    public function hasChanges() {
        return $this->_hasChanges;
    }

    /**
     * Serialize the object.
     *
     * @return string
     */
    public function serialize() {
        return serialize(array('objName' => $this->objName, 'data' => $this->_data, 'options' => $this->_options));
    }

    /**
     * Return the object as JSON.
     *
     * @return string
     */
    public function toJSON() {
        return json_encode(array('objName' => $this->objName, 'data' => $this->_data, 'options' => $this->_options));
    }

    /**
     * Return the object as XML
     *
     * @return string
     */
    public function toXml() {
    }

    /**
     * Return the object as array.
     *
     * @return array
     */
    public function toArray() {
        return array('data' => $this->_data, 'options' => $this->_options);
    }

    /**
     * Return the object as string.
     *
     * @return string
     */
    public function __toString() {
        return implode($this->_data, ', ') . ', ' . implode($this->_options, ', ');
    }

}
