<?php

/**
 * Project:   BTools
 * File:      pr.php 
 * Date:      24.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Funny print_r() substitution with show/hide toggler.
 * Supports multiple arguments.
 * 
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 * @param     One or more arguments for output.
 * @return    void 
 */
function pr() {
    $args = func_get_args();
    $id   = time();
    $id  .= mt_rand(0, 1000);
    
    echo <<<EOT
<script>
    if (typeof toggler !== 'function') {
        function toggler(elId) {
            var prevStat = document.getElementById(elId).style.display;
            if (prevStat == 'none') {
                document.getElementById(elId).style.display = 'block';
                document.getElementById(elId + 'Link').innerText = 'hide';
            } else {
                document.getElementById(elId).style.display = 'none';
                document.getElementById(elId + 'Link').innerText = 'show';
            }
        }
    }
</script>
<div id="doWrapper" style="border: 1px dotted #666; background-color: #CCFFCC;padding: 10px; margin: 5px 0">
    <p style="margin: 0; padding: 0; cursor: pointer" onclick="toggler('do{$id}')" title="Click to toggle display state">
        <b>DEBUG OUTPUT</b> <span style="float: right">(click to <span id="do{$id}Link">hide</span>)</span>
    </p>
    <pre id="do{$id}" style="font-size: 12px;">
EOT;

    $c = count($args);
    for ($i = 0; $i < $c; $i++) {
        var_dump($args[$i]);
        if ($i < $c - 1) {
            print_r('<br />');
        }
    }
    echo '</pre></div>';
}