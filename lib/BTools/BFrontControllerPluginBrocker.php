<?php
/**
 * Project:   BTools
 * File:      BFrontControllerPluginBrocker.php
 * Date:      31.07.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BFrontControllerPluginBrocker
 *
 * @uses      parent_class_name
 * @package   BTools
 * @author    Victor Burak <vb@atompark.com>
 */
class BFrontControllerPluginBrocker
{
    /**
     * @var array
     */
    protected $_plugins = array();

    /**
     * @var BRequest
     */
    protected $_request = NULL;

    /**
     * @var BResponse
     */
    protected $_response = NULL;

    public function __construct() {
        $front = BFrontController::getInstance();
        $this->setRequest($front->getRequest());
        $this->setResponse($front->getResponse());
    }

    public function __call($methodName, $arguments) {
        foreach ($this->_plugins as $plugin) {
            if (method_exists($plugin, $methodName)) {
                $plugin->$methodName($arguments);
            } else {
                throw new Exception('BFrontControllerPluginBrocker: wrong method name!');
            }
        }
    }

    public function registerPlugin(BFrontControllerPluginAbstract $plugin) {
        $plugin->setRequest($this->getRequest());
        $plugin->setResponse($this->getResponse());
        $this->_plugins[get_class($plugin)] = $plugin;
    }

    public function unregisterPlugin($class) {
        if (isset($this->_plugins[$class])) {
            unset($this->_plugins[$class]);
        }
    }

    public function hasPlugin($class) {
        return isset($this->_plugins[$class]) ? TRUE : FALSE;
    }

    public function setRequest(BRequest $request) {
        $this->_request = $request;
        return $this;
    }

    public function getRequest() {
        return $this->_request;
    }

    public function setResponse(BResponse $response) {
        $this->_response = $response;
        return $this;
    }

    public function getResponse() {
        return $this->_response;
    }
}

