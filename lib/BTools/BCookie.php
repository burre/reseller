<?php
/**
 * Project:   BTools
 * File:      BCookie.php
 * Date:      15.08.12
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of BCookie
 *
 */
class BCookie
{
    const BT_COOKIE_SET    = 1;
    const BT_COOKIE_REMOVE = 2;

    public $name;
    public $value;
    public $expire;
    public $path;
    public $domain;
    public $secure;
    public $httponly;

    /**
     * Class constructor.
     *
     * @param        $name
     * @param string $value
     * @param int    $time Time to live in minutes.
     * @param string $path
     * @param string $domain
     * @param bool   $secure
     * @param bool   $httponly
     */
    public function __construct($name,
                                $value = '',
                                $time = 0,
                                $path = '',
                                $domain = '',
                                $secure = FALSE,
                                $httponly = FALSE) {
        $this->name     = $name;
        $this->value    = $value;
        $this->expire   = $time > 0 ? time() + (60 * $time) : $time;
        $this->path     = $path;
        $this->domain   = $domain;
        $this->secure   = $secure;
        $this->httponly = $httponly;
    }

    /**
     * Set cookie.
     *
     * @return bool
     */
    public function set() {
        $_COOKIE[$this->name] = $this->value;
        return setcookie(
            $this->name,
            $this->value,
            $this->expire,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httponly);
    }

    /**
     * Remove cookie.
     *
     * @return bool
     */
    public function remove() {
        unset($_COOKIE[$this->name]);
        return setcookie($this->name, NULL, -1);
    }
}
