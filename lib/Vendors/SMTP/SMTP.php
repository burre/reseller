<?php 
# ------------------------------------------------------------------------------------------------------------------------------------------
/*

Author: Michael Kuznetsov <ursus@tyros.ru>
Support: tyros.ru

”г­ЄжЁп:
        
ADV_mail($host, $user, $pass, $from, $to,  $subject, $mesg, $headers='', $params='')

Desc: ”г­ЄжЁ®­ «м­®Ґ а биЁаҐ­ЁҐ бв ­¤ ав­®© Їа®жҐ¤гал mail
„®Ў ў«Ґ­  ў®§¬®¦­®бвм б®Ґ¤Ё­Ґ­Ёп б бҐаўҐа®¬ Ї® Їа®в®Є®«г RFC 2554

„®Ї®«­ЁвҐ«м­лҐ Ї а ¬Ґвал:

myhost -  Ё¬п е®бв  Ёбв®з­ЁЄ  б®®ЎйҐ­Ёп
smtpport - ­®¬Ґа Ї®ав  SMTP

ђҐ§г«мв в:

‚ б«гз Ґ гбЇҐи­®© ®вЇа ўЄЁ дг­ЄжЁп ў®§ўа й Ґв true. ‚ Їа®вЁў­®¬ б«гз Ґ - ¬ ббЁў б ®ЇЁб ­ЁҐ¬ ®иЁЎЄЁ.

ЏаЁ¬Ґал ЁбЇ®«м§®ў ­Ёп:

ADV_mail('smtp.yandex.ru','user','pasword','from@yandex.ru','to@some.net','Subject','Here your message');

ADV_mail('smtp.yandex.ru','user','pasword','from@yandex.ru','to@some.net','Subject','Here your message',"From: webmaster@$SERVER_NAME\r\n"
    ."Reply-To: webmaster@$SERVER_NAME\r\n"
    ."X-Mailer: PHP/" . phpversion());
        
ADV_mail('smtp.yandex.ru','user','pasword','to@some.net','Subject','Here your message',"From: webmaster@$SERVER_NAME\r\n"
    ."Reply-To: webmaster@$SERVER_NAME\r\n"
    ."X-Mailer: PHP/" . phpversion(), array('myhost'=>'another.net'));
        

*/

function ADV_mail($host, $user, $pass, $from, $to,  $subject, $mesg, $headers='', $params='') {

        # „®Ї®«­ЁвҐ«м­лҐ Ї а ¬Ґвал
        if (!isset($params['myhost'])) $params['myhost']=$_SERVER['HTTP_HOST'];
        if (!isset($params['smtpport'])) $params['smtpport']=0;
        
        if ($headers) $headers.="\r\n\r\n";
        $headers="To: ".$to."\r\nSubject: ".$subject."\r\n".$headers;

        # €­ЁжЁ «Ё§ жЁп
        $amail=new SMTP();
        
        # ‘®Ґ¤Ё­Ґ­ЁҐ б бҐаўҐа®¬
        if ($amail->Connect($host,$params['smtpport'])) {
//print "Connect";
                # ЂгвҐ­вЁдЁЄ жЁп
                if ($amail->AuthHello($params['myhost'], $user, $pass)) {
//print "Auth"; 
                        # Ћв Є®Ј® Ё Є®¬г
                        $amail->MailFrom($from);
                        $amail->Recipient($to);
                
                        # ‘®®ЎйҐ­ЁҐ
                        $amail->Data($headers.$mesg);
                
                        # ‚ле®¤
                        $amail->Quit();
                } else return $amail->error;
        } else return $amail->error;

        $amail->Close();
        
        return true;
}

# ---------------------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------------------------

# SMTP Mail Class
class SMTP {
    var $SMTP_PORT = 25; # the default SMTP PORT
    var $CRLF = "\r\n";  # CRLF pair

    var $smtp_conn;      # the socket to the server
    var $error;          # error if any on the last call
    var $helo_rply;      # the reply the server sent to us for HELO

    var $do_debug;       # the level of debug to perform

    /*
     * SMTP()
     *
     * Initialize the class so that the data is in a known state.
     */
    function SMTP() {
        $this->smtp_conn = 0;
        $this->error = null;
        $this->helo_rply = null;
        $this->do_debug = 0;
    }

    /************************************************************
     *                    CONNECTION FUNCTIONS                  *
     ***********************************************************/


    /*
     * Connected()
     *
     * Returns true if connected to a server otherwise false
     */
    function Connected() {
        if(!empty($this->smtp_conn)) {
            $sock_status = socket_get_status($this->smtp_conn);
            if($sock_status["eof"]) {
                # hmm this is an odd situation... the socket is
                # valid but we aren't connected anymore
                $this->Close();
                return false;
            }
            return true; # everything looks good
        } 
        return false;
    }


    /*
     * Connect($host, $port=0, $tval=30)
     *
     * Connect to the server specified on the port specified.
     * If the port is not specified use the default SMTP_PORT.
     * If tval is specified then a connection will try and be
     * established with the server for that number of seconds.
     * If tval is not specified the default is 30 seconds to
     * try on the connection.
     *
     * SMTP CODE SUCCESS: 220
     * SMTP CODE FAILURE: 421
     */
    function Connect($host,$port=0,$tval=30) {
        # set the error val to null so there is no confusion
        $this->error = null;

        # make sure we are __not__ connected
        if($this->connected()) {
            # ok we are connected! what should we do?
            # for now we will just give an error saying we
            # are already connected
            $this->error =
                array("error" => "Already connected to a server");
            return false;
        }

        if(empty($port)) {
            $port = $this->SMTP_PORT;
        }

        #connect to the smtp server
        $this->smtp_conn = fsockopen($host,    # the host of the server
                                     $port,    # the port to use
                                     $errno,   # error number if any
                                     $errstr,  # error message if any
                                     $tval);   # give up after ? secs
        # verify we connected properly
        if(empty($this->smtp_conn)) {
            $this->error = array("error" => "Failed to connect to server",
                                 "errno" => $errno,
                                 "errstr" => $errstr);
            return false;
        }

        # sometimes the SMTP server takes a little longer to respond
        # so we will give it a longer timeout for the first read
        //if(function_exists("socket_set_timeout"))
        //   socket_set_timeout($this->smtp_conn, 1, 0);

        # get any announcement stuff
        $announce = $this->get_lines();

        # set the timeout  of any socket functions at 1/10 of a second
        //if(function_exists("socket_set_timeout"))
        //   socket_set_timeout($this->smtp_conn, 0, 100000);

        return true;
    }


    /*
     * Close()
     *
     * Closes the socket and cleans up the state of the class.
     * It is not considered good to use this function without
     * first trying to use QUIT.
     */
    function Close() {
        $this->error = null; # so there is no confusion
        $this->helo_rply = null;
        if(!empty($this->smtp_conn)) { 
            # close the connection and cleanup
            fclose($this->smtp_conn);
            $this->smtp_conn = 0;
        }
    }


    /**************************************************************
     *                        SMTP COMMANDS                       *
     *************************************************************/

    /*
     * Data($msg_data)
     *
     * Issues a data command and sends the msg_data to the server
     * finializing the mail transaction. $msg_data is the message
     * that is to be send with the headers. Each header needs to be
     * on a single line followed by a <CRLF> with the message headers
     * and the message body being seperated by and additional <CRLF>.
     *
     * Implements rfc 821: DATA <CRLF>
     *
     * SMTP CODE INTERMEDIATE: 354
     *     [data]
     *     <CRLF>.<CRLF>
     *     SMTP CODE SUCCESS: 250
     *     SMTP CODE FAILURE: 552,554,451,452
     * SMTP CODE FAILURE: 451,554
     * SMTP CODE ERROR  : 500,501,503,421
     */
    function Data($msg_data) {
        $this->error = null; # so no confusion is caused

        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Data() without being connected");
            return false;
        }
                $this->send_line("DATA");

        //fputs($this->smtp_conn,"DATA" . $this->CRLF);

        $rply = $this->get_lines();
        $code = substr($rply,0,3);


        if($code != 354) {
            $this->error =
                array("error" => "DATA command not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }

        # the server is ready to accept data!
        # according to rfc 821 we should not send more than 1000
        # including the CRLF
        # characters on a single line so we will break the data up
        # into lines by \r and/or \n then if needed we will break
        # each of those into smaller lines to fit within the limit.
        # in addition we will be looking for lines that start with
        # a period '.' and append and additional period '.' to that
        # line. NOTE: this does not count towards are limit.

        # normalize the line breaks so we know the explode works
        $msg_data = str_replace("\r\n","\n",$msg_data);
        $msg_data = str_replace("\r","\n",$msg_data);
        $lines = explode("\n",$msg_data);

        # we need to find a good way to determine is headers are
        # in the msg_data or if it is a straight msg body
        # currently I'm assuming rfc 822 definitions of msg headers
        # and if the first field of the first line (':' sperated)
        # does not contain a space then it _should_ be a header
        # and we can process all lines before a blank "" line as
        # headers.
        $field = substr($lines[0],0,strpos($lines[0],":"));
        $in_headers = false;
        if(!empty($field) && !strstr($field," ")) {
            $in_headers = true;
        }

        $max_line_length = 998; # used below; set here for ease in change

        while(list(,$line) = @each($lines)) {
            $lines_out = null;
            if($line == "" && $in_headers) {
                $in_headers = false;
            }
            # ok we need to break this line up into several
            # smaller lines
            while(strlen($line) > $max_line_length) {
                $pos = strrpos(substr($line,0,$max_line_length)," ");
                $lines_out[] = substr($line,0,$pos);
                $line = substr($line,$pos + 1);
                # if we are processing headers we need to
                # add a LWSP-char to the front of the new line
                # rfc 822 on long msg headers
                if($in_headers) {
                    $line = "\t" . $line;
                }
            }
            $lines_out[] = $line;

            # now send the lines to the server
            while(list(,$line_out) = @each($lines_out)) {
                if(@$line_out[0] == ".") {
                    $line_out = "." . $line_out;
                }
                @$tmpdata .= $line_out.$this->CRLF;
            }
        }
        # ok all the message data has been sent so lets get this
        # over with aleady
                $this->send_line($tmpdata.$this->CRLF.".");
        //fputs($this->smtp_conn, $this->CRLF . "." . $this->CRLF);

        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 250) {
            $this->error =
                array("error" => "DATA not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        return true;
    }


    /*
     * Hello($host="")
     *
     * Sends the HELO command to the smtp server.
     * This makes sure that we and the server are in
     * the same known state.
     *
     * Implements from rfc 821: HELO <SP> <domain> <CRLF>
     *
     * SMTP CODE SUCCESS: 250
     * SMTP CODE ERROR  : 500, 501, 504, 421
     */
    function Hello($host="") {
        $this->error = null; # so no confusion is caused
        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Hello() without being connected");
            return false;
        }
        # if a hostname for the HELO wasn't specified determine
        # a suitable one to send
        if(empty($host)) {
            # we need to determine some sort of appopiate default
            # to send to the server
            $host = "localhost";
        }

                $this->send_line("HELO " . $host);

        //fputs($this->smtp_conn,"HELO " . $host . $this->CRLF);
                
        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 250) {
            $this->error =
                array("error" => "HELO not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        $this->helo_rply = $rply;

        return true;
    }

    function AuthHello($host="",$user="",$pass="") {

        $this->error = null; # so no confusion is caused
        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Hello() without being connected");
            return false;
        }
        # if a hostname for the HELO wasn't specified determine
        # a suitable one to send
        if(empty($host)) {
            # we need to determine some sort of appopiate default
            # to send to the server
            $host = "localhost";
        }

        $this->send_line("EHLO ".$host);

        $rply = $this->get_lines();
        $code = substr($rply,0,3);
        if($code != 250) {
            $this->error =
                array("error" => "EHLO not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        $this->helo_rply = $rply;
        $this->send_line("AUTH LOGIN");
        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 334) {
            $this->error =
                array("error" => "AUTH LOGIN not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }

        $this->send_line(base64_encode($user));
        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 334) {
            $this->error =
                array("error" => "USER not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }

        $this->send_line(base64_encode($pass));
        $rply = $this->get_lines();
        $code = substr($rply,0,3);
        if($code != 235) {
            $this->error =
                array("error" => "PASSWORD not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        return true;
    }

    /*
     * MailFrom($from)
     *
     * Starts a mail transaction from the email address specified in
     * $from. Returns true if successful or false otherwise. If True
     * the mail transaction is started and then one or more Recipient
     * commands may be called followed by a Data command.
     *
     * Implements rfc 821: MAIL <SP> FROM:<reverse-path> <CRLF>
     *
     * SMTP CODE SUCCESS: 250
     * SMTP CODE SUCCESS: 552,451,452
     * SMTP CODE SUCCESS: 500,501,421
     */
    function MailFrom($from) {
        $this->error = null; # so no confusion is caused

        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Mail() without being connected");
            return false;
        }

        $this->send_line("MAIL FROM:" . $from);
                
        //fputs($this->smtp_conn,"MAIL FROM:" . $from . $this->CRLF);

        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 250) {
            $this->error =
                array("error" => "MAIL not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        return true;
    }



    /*
     * Quit($close_on_error=true)
     *
     * Sends the quit command to the server and then closes the socket
     * if there is no error or the $close_on_error argument is true.
     *
     * Implements from rfc 821: QUIT <CRLF>
     *
     * SMTP CODE SUCCESS: 221
     * SMTP CODE ERROR  : 500
     */
    function Quit($close_on_error=true) {
        $this->error = null; # so there is no confusion

        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Quit() without being connected");
            return false;
        }

        # send the quit command to the server
        $this->send_line("QUIT");
        //fputs($this->smtp_conn,"quit" . $this->CRLF);

        # get any good-bye messages
        $byemsg = $this->get_lines();

        $rval = true;
        $e = null;

        $code = substr($byemsg,0,3);
        if($code != 221) {
            # use e as a tmp var cause Close will overwrite $this->error
            $e = array("error" => "SMTP server rejected quit command",
                       "smtp_code" => $code,
                       "smtp_rply" => substr($byemsg,4));
            $rval = false;
        }

        if(empty($e) || $close_on_error) {
            $this->Close();
        }

        return $rval;
    }

    /*
     * Recipient($to)
     *
     * Sends the command RCPT to the SMTP server with the TO: argument of $to.
     * Returns true if the recipient was accepted false if it was rejected.
     *
     * Implements from rfc 821: RCPT <SP> TO:<forward-path> <CRLF>
     *
     * SMTP CODE SUCCESS: 250,251
     * SMTP CODE FAILURE: 550,551,552,553,450,451,452
     * SMTP CODE ERROR  : 500,501,503,421
     */
    function Recipient($to) {
        $this->error = null; # so no confusion is caused

        if(!$this->connected()) {
            $this->error = array(
                    "error" => "Called Recipient() without being connected");
            return false;
        }

        $this->send_line("RCPT TO:" . $to);
        //fputs($this->smtp_conn,"RCPT TO:" . $to . $this->CRLF);

        $rply = $this->get_lines();
        $code = substr($rply,0,3);

        if($code != 250 && $code != 251) {
            $this->error =
                array("error" => "RCPT not accepted from server",
                      "smtp_code" => $code,
                      "smtp_msg" => substr($rply,4));
            return false;
        }
        return true;
    }

    function get_lines() {
        $data = "";
        while($str = fgets($this->smtp_conn,515)) {
            $data .= $str;
            # if the 4th character is a space then we are done reading
            # so just break the loop
            if(substr($str,3,1) == " ") { break; }
       }
         if($this->do_debug) {
                        $tmp = ereg_replace("(\r|\n)","",$data);
                        echo("<font style=\"font-size:12px; font-family: Courier New; background-color: white; color: black;\"><- <b>".htmlspecialchars($tmp)."</b></font><br>\r\n");flush();
         }
        //echo "$data<br>";
        return $data;
    }

    function send_line($data) {
                //echo "$data <br>";
                fputs($this->smtp_conn,$data.$this->CRLF);
                if($this->do_debug) {
                        $data = htmlspecialchars($data);
                        echo("<font style=\"font-size:12px; font-family: Courier New; background-color: white; color: black;\">-> ".nl2br($data)."</font><br>\r\n");flush();
                }
    }

}


/************************************************************************
UebiMiau is a GPL'ed software developed by 

 - Aldoir Ventura - aldoir@users.sourceforge.net
 - http://uebimiau.sourceforge.net

Fell free to contact, send donations or anything to me :-)
SЈo Paulo - Brasil
*************************************************************************/

?>
