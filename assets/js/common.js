/**
 * Project:   Atomic Reseller
 * File:      common.js
 * Date:      30.07.12
 *
 * @author    Victor Burak <vb@atompark.com>
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

$(document).ready(function () {

});


/**
 * INDEX CONTROLLER
 */

/**
 * Open login dialog.
 * @see navbar.phtml
 */
function loginModal() {
    $('#login-modal').modal({keyboard:true});
    $('#login').focus();
}

/**
 * ORDERS CONTROLLER
 */

/**
 * Delete order dialog.
 * @param orderNum
 * @see delete-order-modal.phtml,orders/index.phtml
 */
function deleteOrderModal(orderNum) {
    $('#order-to-delete').text(orderNum);
    $('#delete-order-modal').modal({keyboard:true});
}

/**
 * Delete order via AJAX.
 * @param orderNum
 * @see delete-order-modal.phtml,orders/index.phtml
 */
function deleteOrder(orderNum) {
    console.log('deleteOrder');
}

function setNoticeModal(orderNum) {
    console.log('setNotice');
}

function sendKeys(orderNum) {
    console.log('sendKeys');
}

function showNotices() {
    $("#show-notices-modal").modal({keyboard:true});
}
