<?php

/**
 * Project:   ePochta Reseller
 * File:      index.php
 * Date:      16.07.2012
 *
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Handles main application.
 *
 * @author    Victor Burak <vb@atompark.com>
 */

define('BT_DEBUG_MODE', TRUE);

require_once('app/bootstrap.php');
